package project.application.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.GetPersonInfoResponseDTO;
import project.dto.PersonsResponseDTO;
import project.dto.TransactionDTO;
import project.dto.TransactionsResponseDTO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FilterPersonByNameServiceIT {

    @Autowired
    private FilterPersonByNameService service;

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void constructorHappyCaseTest() {
        // Arrange
        // Act
        // Assert
        assertTrue(service instanceof FilterPersonByNameService);
    }

    @Test
    void filterPersonByNameWithResults() {
        // Arrange
        GetPersonInfoResponseDTO maria = new GetPersonInfoResponseDTO("4004@switch.pt","maria", "Rua Santa Catarina 35","1989-12-18","Porto");

        List<GetPersonInfoResponseDTO> expectedString = new ArrayList<>();
        expectedString.add(maria);


        PersonsResponseDTO expected = new PersonsResponseDTO(expectedString);

        // Act
        PersonsResponseDTO actual = service.filterPersonByName("ria");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void filterPersonByNameWithNoResults() {
        // Arrange
        List<GetPersonInfoResponseDTO> expectedString = new ArrayList<>();
        PersonsResponseDTO expected = new PersonsResponseDTO(expectedString);

        // Act
        PersonsResponseDTO actual = service.filterPersonByName("abc");

        // Assert
        assertEquals(expected, actual);
    }
}