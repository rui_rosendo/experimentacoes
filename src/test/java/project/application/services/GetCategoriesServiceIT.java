package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.CategoriesDTO;
import project.dto.CategoryDTO;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSGetCategoriesService;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class GetCategoriesServiceIT {

    @Autowired
    IUSGetCategoriesService service;

    /**
     * Test for getCategoriesByGroupID Integration Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for getCategoriesByGroupID - Happy Case")
    void getCategoriesByGroupIDHappyCaseTest() {
        //Arrange
        Set<CategoryDTO> categories = new HashSet<>();
        CategoryDTO category1 = new CategoryDTO("Arbitros");
        categories.add(category1);

        CategoriesDTO categoriesDTO = new CategoriesDTO(categories);

        //Act
        CategoriesDTO result = service.getCategoriesByGroupID("510200");

        //Assert
        assertEquals(categoriesDTO, result);
    }

    /**
     * Test for getCategoriesByGroupID Integration Test
     * Ensure GroupNotFoundException
     */
    @Test
    @DisplayName("Test for getCategoriesByGroupID - Ensure GroupNotFoundException")
    void getCategoriesByGroupIDEnsureGroupNotFoundExceptionTest() {
        //Arrange
        //Act
        //Assert
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getCategoriesByGroupID("1");
        });
    }

    /**
     * Test for getCategoriesByPersonID Integration Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for getCategoriesByPersonID - Happy Case")
    void getCategoriesByPersonIDHappyCaseTest() {
        //Arrange
        Set<CategoryDTO> categories = new HashSet<>();
        CategoryDTO category1 = new CategoryDTO("Cerveja");
        categories.add(category1);

        CategoriesDTO categoriesDTO = new CategoriesDTO(categories);

        //Act
        CategoriesDTO result = service.getCategoriesByPersonID("51010@switch.pt");

        //Assert
        assertEquals(categoriesDTO, result);
    }

    /**
     * Test for getCategoriesByPersonID Integration Test
     * Ensure PersonNotFoundException
     */
    @Test
    @DisplayName("Test for getCategoriesByPersonID - Ensure PersonNotFoundException")
    void getCategoriesByPersonIDEnsurePersonNotFoundExceptionTest() {
        //Arrange
        //Act
        //Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.getCategoriesByPersonID("1@switch.pt");
        });
    }
}