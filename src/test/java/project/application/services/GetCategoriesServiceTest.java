package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CategoriesDTO;
import project.dto.CategoryDTO;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class GetCategoriesServiceTest {

    @Autowired
    GetCategoriesService service;

    @Autowired
    GroupRepository groupRepo;

    @Autowired
    PersonRepository personRepo;

    /**
     * Test for getCategoriesByGroupID Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Test for getCategoriesByGroupID - Happy Case")
    public void getCategoriesByGroupIDHappyCaseTest() {

        //ARRANGE
        GroupID gID = new GroupID("510100");
        PersonID creatorID = new PersonID("51010@switch.pt");
        LedgerID ledgerID = new LedgerID("51020");

        Group groupUS5 = new Group(gID, "Futebol", "2020-01-20", creatorID, ledgerID);
        groupUS5.addCategory("Equipamentos");

        CategoryDTO category1 = new CategoryDTO("Equipamentos");
        Set<CategoryDTO> categoriesDTO = new HashSet<>();
        categoriesDTO.add(category1);

        CategoriesDTO expectedCategoriesDTO = new CategoriesDTO(categoriesDTO);

        Mockito.when(groupRepo.findById(gID)).thenReturn(Optional.of(groupUS5));

        //ACT
        CategoriesDTO result = service.getCategoriesByGroupID("510100");

        //ASSERT
        assertEquals(expectedCategoriesDTO, result);

    }

    /**
     * Test for getCategoriesByGroupID Unit Test
     * Ensure GroupNotFoundException
     */
    @Test
    @DisplayName("Test for getCategoriesByGroupID - Ensure GroupNotFoundException")
    public void getCategoriesByGroupIDEnsureGroupNotFoundExceptionTest() {

        //ARRANGE
        String gIDString = "510100";
        GroupID gID = new GroupID(gIDString);
        Mockito.when(groupRepo.findById(gID)).thenReturn(Optional.empty());

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getCategoriesByGroupID(gIDString);
        });
    }

    /**
     * Test for getCategoriesByPersonID Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Test for getCategoriesByPersonID - Happy Case")
    public void getCategoriesByPersonIDHappyCaseTest() {

        //ARRANGE
        String personIDString = "510100@switch.pt";
        PersonID personID = new PersonID(personIDString);
        LedgerID ledgerID = new LedgerID("51020");

        Person person = new Person(personID, "Antonio", "Rua das Valongas", "Porto", "2020-01-20", null, null, ledgerID);
        person.addCategory("Equipamentos");

        CategoryDTO category1 = new CategoryDTO("Equipamentos");
        Set<CategoryDTO> categoriesDTO = new HashSet<>();
        categoriesDTO.add(category1);

        CategoriesDTO expectedCategoriesDTO = new CategoriesDTO(categoriesDTO);

        Mockito.when(personRepo.findById(personID)).thenReturn(Optional.of(person));

        //ACT
        CategoriesDTO result = service.getCategoriesByPersonID(personIDString);

        //ASSERT
        assertEquals(expectedCategoriesDTO, result);

    }

    /**
     * Test for getCategoriesByPersonID Unit Test
     * Ensure PersonNotFoundException
     */
    @Test
    @DisplayName("Test for getCategoriesByGroupID - Ensure GroupNotFoundException")
    public void getCategoriesByGroupIDEnsurePersonNotFoundExceptionTest() {

        //ARRANGE
        String personIDString = "510100@switch.pt";
        PersonID pID = new PersonID(personIDString);
        Mockito.when(personRepo.findById(pID)).thenReturn(Optional.empty());

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.getCategoriesByPersonID(personIDString);
        });
    }
}
