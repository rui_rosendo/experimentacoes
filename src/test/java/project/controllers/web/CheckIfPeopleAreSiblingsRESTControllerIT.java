package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CheckIfPeopleAreSiblingsRESTControllerIT extends AbstractTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * PersonEmail1 10005@switch.pt and PersonEmail2 10006@switch.pt have the same mother.
     * Ensures it returns true when two people have the same mother.
     * Expected content true.
     */
    @Test
    @DisplayName("Ensure true with same mother test")
    void ensureTrueWithSameMotherTest() throws Exception {
        //Arrange
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID1", "10005@switch.pt");
        pathVariableMap.put("personID2", "10006@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID1}/siblings/{personID2}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = HttpStatus.OK.value();
        JSONObject expectedContent = new JSONObject()
                .put("isSibling", "true");

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * PersonEmail1 10005@switch.pt and PersonEmail2 10007@switch.pt have the same father.
     * Ensures it returns true when two people have the same father.
     * Expected content true.
     */
    @Test
    @DisplayName("Ensure true with same father test")
    void ensureTrueWithSameFatherTest() throws Exception {
        //Arrange
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID1", "10005@switch.pt");
        pathVariableMap.put("personID2", "10007@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID1}/siblings/{personID2}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = HttpStatus.OK.value();
        JSONObject expectedContent = new JSONObject()
                .put("isSibling", "true");

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * PersonEmail1 10006@switch.pt and PersonEmail2 10007@switch.pt are in each other's siblings list.
     * Ensures it returns true when two people are in each other's siblings list.
     * Expected content boolean true.
     */
    @Test
    @DisplayName("Ensure true by siblings list: Person2 is present in Person1 Siblings list")
    void ensureTrueBySiblingsListTest() throws Exception {
        //Arrange
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID1", "10006@switch.pt");
        pathVariableMap.put("personID2", "10007@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID1}/siblings/{personID2}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = HttpStatus.OK.value();
        JSONObject expectedContent = new JSONObject()
                .put("isSibling", "true");

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * PersonEmail1 10007@switch.pt and PersonEmail2 10006@switch.pt are in each other's siblings list.
     * Ensures it returns true when two people are in each other's siblings list,
     * whatever the order of the parameters inputted (reversing the order).
     * Expected content boolean true.
     */
    @Test
    @DisplayName("Ensure true by siblings list: Person1 is present in Person2 Siblings list")
    void ensureTrueByReciprocalSiblingsListTest() throws Exception {
        //Arrange
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID1", "10007@switch.pt");
        pathVariableMap.put("personID2", "10006@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID1}/siblings/{personID2}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = HttpStatus.OK.value();
        JSONObject expectedContent = new JSONObject()
                .put("isSibling", "true");

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Ensures it returns false when two people are not in each other's siblings list,
     * don´t have the same father and don´t have the same mother.
     * Expected content boolean false.
     */
    @Test
    @DisplayName("Ensure false by mother and father and siblings list: people don't have the same mother, father or are present in the siblings list")
    void ensureFalseByMotherAndFatherAndSiblingsListTest() throws Exception {
        //Arrange
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID1", "10007@switch.pt");
        pathVariableMap.put("personID2", "10008@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID1}/siblings/{personID2}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = HttpStatus.OK.value();
        JSONObject expectedContent = new JSONObject()
                .put("isSibling", "false");

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Assert PersonNotFoundException is thrown if person at least one of the person not in the Person Repository.
     */
    @Test
    @DisplayName("Ensure fails when person not found in repository")
    void ensureFailsWhenPersonNotFoundTest() throws Exception {
        //Arrange
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID1", "10007@switch.pt");
        pathVariableMap.put("personID2", "10009@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID1}/siblings/{personID2}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Person not found")
                .put("errors", new JSONArray(Collections.singletonList("Person not found")));

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).
                accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);

    }

}