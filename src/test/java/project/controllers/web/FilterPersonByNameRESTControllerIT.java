package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FilterPersonByNameRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    @Test
    void filterPersonByNameTestNoResults() throws Exception{
        String url = "/people/filterName";
        int expectedStatus = 200;

        JSONObject expectedContent =  new JSONObject().put("persons", new JSONArray());

    //Act
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(url)
            .param("filter","abc"))
            .andReturn();
    int actualStatus = mvcResult.getResponse().getStatus();
    JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());


    //Assert
    JSONAssert.assertEquals(expectedContent, actualContent,true);
    assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void filterPersonByNameTestWithResults() throws Exception{
        String url = "/people/filterName";
        int expectedStatus = 200;

        JSONObject expectedContent =  new JSONObject().put("persons", new JSONArray(Arrays.asList(
                new JSONObject()
                        .put("email", "4004@switch.pt")
        .put("name", "maria")
        .put("address", "Rua Santa Catarina 35")
        .put("birthplace", "Porto")
        .put("birthDate", "1989-12-18"))));

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(url)
                .param("filter","ria"))
                .andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());


        //Assert
        JSONAssert.assertEquals(expectedContent, actualContent,true);
        assertEquals(expectedStatus, actualStatus);
    }
}