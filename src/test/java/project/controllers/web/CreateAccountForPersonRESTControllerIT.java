package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;
import project.dto.CreateAccountForPersonInfoDTO;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateAccountForPersonRESTControllerIT extends AbstractTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Autowired
    AccountRepository accountRepository;

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * createAccountForPerson
     * Happy Case
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("createAccountForPerson - Happy Case")
    public void createAccountForPersonHappyCaseTest() throws Exception {

        // Global variables to be used throughout the test
        String personID = "6662@switch.pt";
        String personName = "Ripley, Ellen";
        String accountID = "66607";
        String denomination = "flamethrowers";
        String description = "gear";

        // GET ACCOUNT THAT DOESN'T EXIST
        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("accountID", accountID);

        String getURI = UriComponentsBuilder.fromUriString("")
                .path("/accounts/{accountID}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        CreateAccountForPersonInfoDTO createAccountForPersonInfoDTO = new CreateAccountForPersonInfoDTO(accountID, denomination, description);
        String inputJson = super.mapToJson(createAccountForPersonInfoDTO);

        int expectedStatus1 = 404;
        JSONObject expectedContent1 = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Account not found")
                .put("errors", new JSONArray(Collections.singletonList("Account not found")));

        // ACT
        MvcResult getNegativeMvcResult = mvc.perform(MockMvcRequestBuilders.get(getURI).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus1 = getNegativeMvcResult.getResponse().getStatus();
        JSONObject actualContent1 = new JSONObject(getNegativeMvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus1, actualStatus1);
        JSONAssert.assertEquals(expectedContent1, actualContent1, true);

        // CREATE NEW ACCOUNT
        // ARRANGE
        Map<String, Object> pathVariableMap2 = new HashMap<>();
        pathVariableMap2.put("personID", personID);

        String postURI = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/accounts")
                .buildAndExpand(pathVariableMap2)
                .toUriString();

        int expectedStatus2 = 201;
        JSONObject expectedContent2 = new JSONObject()
                .put("personEmail", personID)
                .put("personName", personName)
                .put("accountID", accountID)
                .put("accountDenomination", denomination)
                .put("_links", new JSONObject()
                        .put("self", new JSONObject().
                                put("href", "http://localhost/accounts/" + accountID))
                        .put("accounts", new JSONObject().
                                put("href", "http://localhost/people/" + personID + "/accounts")));

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(postURI).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus2 = mvcResult.getResponse().getStatus();
        JSONObject actualContent2 = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus2, actualStatus2);
        JSONAssert.assertEquals(expectedContent2, actualContent2, true);

        // GET ACCOUNT THAT WAS CREATED
        // ARRANGE
        int expectedStatus3 = 200;
        JSONObject expectedContent3 = new JSONObject()
                .put("accountID", accountID)
                .put("description", description)
                .put("denomination", denomination);

        // ACT
        MvcResult getPositiveMvcResult = mvc.perform(MockMvcRequestBuilders.get(getURI).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus3 = getPositiveMvcResult.getResponse().getStatus();
        JSONObject actualContent3 = new JSONObject(getPositiveMvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus3, actualStatus3);
        JSONAssert.assertEquals(expectedContent3, actualContent3, true);
    }

    /**
     * Method createAccountForPerson
     * person ID doesn't exist in the database
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("createAccountForPerson - person ID doesn't exist in the database")
    public void createAccountForPersonRepositoryDontContainsPersonTest() throws Exception {

        // Global variables to be used throughout the test
        String personID = "6668@switch.pt";
        String accountID = "66609";
        String denomination = "shopping";
        String description = "food";

        // GET ACCOUNT THAT DOESN'T EXIST
        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("accountID", accountID);

        String getURI = UriComponentsBuilder.fromUriString("")
                .path("/accounts/{accountID}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        CreateAccountForPersonInfoDTO createAccountForPersonInfoDTO = new CreateAccountForPersonInfoDTO(accountID, denomination, description);
        String inputJson = super.mapToJson(createAccountForPersonInfoDTO);

        // ACT
        int expectedStatus1 = 404;
        JSONObject expectedContent1 = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Account not found")
                .put("errors", new JSONArray(Collections.singletonList("Account not found")));

        // ACT
        MvcResult getNegativeMvcResult = mvc.perform(MockMvcRequestBuilders.get(getURI).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus1 = getNegativeMvcResult.getResponse().getStatus();
        JSONObject actualContent1 = new JSONObject(getNegativeMvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus1, actualStatus1);
        JSONAssert.assertEquals(expectedContent1, actualContent1, true);

        // CREATE ACCOUNT FOR PERSON THAT DOESN'T EXIST
        // ARRANGE
        Map<String, Object> pathVariableMap2 = new HashMap<>();
        pathVariableMap2.put("personID", personID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/accounts")
                .buildAndExpand(pathVariableMap2)
                .toUriString();

        int expectedStatus2 = 404;
        JSONObject expectedContent2 = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Person not found")
                .put("errors", new JSONArray(Collections.singletonList("Person not found")));

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus2 = mvcResult.getResponse().getStatus();
        JSONObject actualContent2 = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus2, actualStatus2);
        JSONAssert.assertEquals(expectedContent2, actualContent2, true);

    }

    /**
     * createAccountForPerson
     * Account ID already exists in the database
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("createAccountForPerson - Account ID already exists in the database")
    public void createAccountForPersonAccountAlreadyExistsInTheAccountRepositoryAndInThePersonTest() throws Exception {
        // Global variables to be used throughout the test
        String personID = "6661@switch.pt";
        String accountID = "66602";
        String denomination = "spaceships";
        String description = "Space race";

        // GET ACCOUNT THAT DOESN'T EXIST
        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("accountID", accountID);

        String getURI = UriComponentsBuilder.fromUriString("")
                .path("/accounts/{accountID}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        CreateAccountForPersonInfoDTO createAccountForPersonInfoDTO = new CreateAccountForPersonInfoDTO(accountID, denomination, description);
        String inputJson = super.mapToJson(createAccountForPersonInfoDTO);

        // ACT
        int expectedStatus1 = 200;
        JSONObject expectedContent1 = new JSONObject()
                .put("accountID", accountID)
                .put("description", description)
                .put("denomination", denomination);

        // ACT
        MvcResult getNegativeMvcResult = mvc.perform(MockMvcRequestBuilders.get(getURI).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus1 = getNegativeMvcResult.getResponse().getStatus();
        JSONObject actualContent1 = new JSONObject(getNegativeMvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus1, actualStatus1);
        JSONAssert.assertEquals(expectedContent1, actualContent1, true);

        // CREATE NEW ACCOUNT WITH SAME ID
        // ARRANGE
        Map<String, Object> pathVariableMap2 = new HashMap<>();
        pathVariableMap2.put("personID", personID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/accounts")
                .buildAndExpand(pathVariableMap2)
                .toUriString();

        int expectedStatus2 = 422;
        JSONObject expectedContent2 = new JSONObject()
                .put("status", "UNPROCESSABLE_ENTITY")
                .put("message", "Account already exists")
                .put("errors", new JSONArray(Collections.singletonList("Account already exists")));

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus2 = mvcResult.getResponse().getStatus();
        JSONObject actualContent2 = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus2, actualStatus2);
        JSONAssert.assertEquals(expectedContent2, actualContent2, true);

    }

    /**
     * getAccountsByPersonID
     * Happy Case
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("getAccountByPersonID - HappyCase")
    public void getAccountsByPersonIDHappyCaseTest() throws Exception {
        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", "6661@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/accounts")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 200;
        JSONObject expectedContent = new JSONObject()
                .put("accounts", new JSONArray(Arrays.asList(
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/accounts/66601")))
                                .put("denomination", "arrows")
                                .put("description", "War assets")
                                .put("accountID", "66601"),
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/accounts/66606")))
                                .put("denomination", "knife sharpeners")
                                .put("description", "More war assets")
                                .put("accountID", "66606")
                        ))
                );

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);

    }

    /**
     * getAccountsByPersonID - Sad Case
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("getAccountsByPersonID - person ID doesn't exist in the database")
    public void getAccountsByPersonIDSadCaseWhenPersonDoesNotExistInRepoTest() throws Exception {
        // ARRANGE
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", "6668@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/accounts")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Person not found")
                .put("errors", new JSONArray(Collections.singletonList("Person not found")));

        // ACT
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

}