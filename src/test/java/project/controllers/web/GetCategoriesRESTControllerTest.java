package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.application.services.GetCategoriesService;
import project.dto.CategoriesDTO;
import project.dto.CategoryDTO;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetCategoriesRESTControllerTest {

    @Autowired
    private GetCategoriesService service;

    @Autowired
    private GetCategoriesRESTController controller;

    /**
     * getCategoriesByGroupID - Happy Case
     */
    @Test
    @DisplayName("getCategoriesByGroupID - Happy Case")
    public void getCategoryByGroupIDHappyCaseTest() {

        //ARRANGE
        String groupID = "510200";


        CategoryDTO category1 = new CategoryDTO("equipamento");
        Set<CategoryDTO> categoriesDTO = new HashSet<>();
        categoriesDTO.add(category1);

        CategoriesDTO expected = new CategoriesDTO(categoriesDTO);

        Mockito.when(service.getCategoriesByGroupID(groupID)).thenReturn(expected);

        //ACT
        Object result = controller.getCategoriesByGroupID(groupID).getBody();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * getCategoriesByPersonID - Happy Case
     */
    @Test
    @DisplayName("getCategoriesByPersonID - Happy Case")
    public void getCategoryByPersonIDHappyCaseTest() {

        //ARRANGE
        String personID = "510200";


        CategoryDTO category1 = new CategoryDTO("equipamento");
        Set<CategoryDTO> categoriesDTO = new HashSet<>();
        categoriesDTO.add(category1);

        CategoriesDTO expected = new CategoriesDTO(categoriesDTO);

        Mockito.when(service.getCategoriesByPersonID(personID)).thenReturn(expected);

        //ACT
        Object result = controller.getCategoriesByPersonID(personID).getBody();

        //ASSERT
        assertEquals(expected, result);
    }

}