package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.TransactionsAssembler;
import project.dto.TransactionsResponseDTO;
import project.frameworkddd.IUSGetTransactionsService;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetTransactionsRESTControllerTest {

    @Autowired
    private IUSGetTransactionsService service;

    @Autowired
    private GetTransactionsRESTController controller;

    /**
     * Unit Test getTransactionsByPersonID - Happy Case Test
     */
    @Test
    @DisplayName("getTransactionsByPersonID - Happy Case")
    void getTransactionsByPersonIDHappyCaseTest() {
        //Arrange
        String personId = "1001@switch.com";

        Transaction transaction = new Transaction("1000", Type.DEBIT, "2020-01-01 12:00:00", "description",
                new Category("category"), new AccountID("500"), new AccountID("600"));
        List<Transaction> transactionsSet = new ArrayList<>();
        transactionsSet.add(transaction);
        TransactionsResponseDTO responseDTO = TransactionsAssembler.mapToDTO(transactionsSet);

        Mockito.when(service.getTransactionsByPersonID(personId)).thenReturn(responseDTO);

        //Act
        ResponseEntity<Object> actualResponse = controller.getTransactionsByPersonID(personId);
        Object actualBody = actualResponse.getBody();
        Object actualStatus = actualResponse.getStatusCode();

        //Assert
        assertEquals(responseDTO, actualBody);
        assertEquals(HttpStatus.OK, actualStatus);
    }

    /**
     * Unit Test getTransactionsByGroupID - Happy Case Test
     */
    @Test
    @DisplayName("getTransactionByGroupID - Happy Case")
    void getTransactionByGroupIDHappyCaseTest() {
        //Arrange
        String personId = "8101@switch.com";
        String groupId = "8101";

        Transaction transaction = new Transaction("1000", Type.DEBIT, "2020-01-01 12:00:00", "description",
                new Category("category"), new AccountID("500"), new AccountID("600"));
        List<Transaction> transactionsSet = new ArrayList<>();
        transactionsSet.add(transaction);
        TransactionsResponseDTO responseDTO = TransactionsAssembler.mapToDTO(transactionsSet);

        Mockito.when(service.getTransactionsByGroupID(personId, groupId)).thenReturn(responseDTO);

        //Act
        ResponseEntity<Object> actualResponse = controller.getTransactionsByGroupID(personId, groupId);
        Object actualBody = actualResponse.getBody();
        Object actualStatus = actualResponse.getStatusCode();

        //Assert
        assertEquals(responseDTO, actualBody);
        assertEquals(HttpStatus.OK, actualStatus);
    }

    /**
     * Unit Test getTransactionsOfAccountWithinPeriodByPersonID - Happy Case Test
     */
    @Test
    @DisplayName("getTransactionsOfAccountWithinPeriodByPersonID - Happy Case")
    void getTransactionsOfAccountWithinPeriodByPersonIDHappyCaseTest() {
        //Arrange
        String personId = "1001@switch.com";
        String accountId = "1001";
        String initialDate = "1999-01-01 12:00:00";
        String finalDate = "2021-01-01 12:00:00";

        Transaction transaction = new Transaction("1000", Type.DEBIT, "2020-01-01 12:00:00", "description",
                new Category("category"), new AccountID("500"), new AccountID("600"));
        List<Transaction> transactionsSet = new ArrayList<>();
        transactionsSet.add(transaction);
        TransactionsResponseDTO responseDTO = TransactionsAssembler.mapToDTO(transactionsSet);

        Mockito.when(service.getTransactionsOfAccountWithinPeriodByPersonID(personId, accountId, initialDate, finalDate)).thenReturn(responseDTO);

        //Act
        ResponseEntity<Object> actualResponse = controller.getTransactionsOfAccountWithinPeriodByPersonID(personId, accountId, initialDate, finalDate);
        Object actualBody = actualResponse.getBody();
        Object actualStatus = actualResponse.getStatusCode();

        //Assert
        assertEquals(responseDTO, actualBody);
        assertEquals(HttpStatus.OK, actualStatus);
    }

    /**
     * Unit Test getTransactionsOfAccountWithinPeriodByGroupID - Happy Case Test
     */
    @Test
    @DisplayName("getTransactionsOfAccountWithinPeriodByGroupID - Happy Case")
    void getTransactionsOfAccountWithinPeriodByGroupIDHappyCaseTest() {
        //Arrange
        String personId = "1001@switch.com";
        String groupId = "1002";
        String accountId = "1001";
        String initialDate = "1999-01-01 12:00:00";
        String finalDate = "2021-01-01 12:00:00";

        Transaction transaction = new Transaction("1000", Type.DEBIT, "2020-01-01 12:00:00", "description",
                new Category("category"), new AccountID("500"), new AccountID("600"));
        List<Transaction> transactionsSet = new ArrayList<>();
        transactionsSet.add(transaction);
        TransactionsResponseDTO responseDTO = TransactionsAssembler.mapToDTO(transactionsSet);

        Mockito.when(service.getTransactionsOfAccountWithinPeriodByGroupID(personId, groupId, accountId, initialDate, finalDate)).thenReturn(responseDTO);

        //Act
        ResponseEntity<Object> actualResponse = controller.getTransactionsOfAccountWithinPeriodByGroupID(personId, groupId, accountId, initialDate, finalDate);
        Object actualBody = actualResponse.getBody();
        Object actualStatus = actualResponse.getStatusCode();

        //Assert
        assertEquals(responseDTO, actualBody);
        assertEquals(HttpStatus.OK, actualStatus);
    }
}