package project.controllers.web;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.application.services.CreateAccountForGroupService;
import project.dto.*;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.AccountNotFoundException;
import project.exceptions.GroupConflictException;
import project.exceptions.GroupNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class CreateAccountForGroupRESTControllerTest {

    @Autowired
    private CreateAccountForGroupService service;

    @Autowired
    private CreateAccountForGroupRESTController controller;

    /**
     * CreateAccountForGroup Happy case
     */
    @Test
    @DisplayName("Create Account for Group - happy case")
    public void createAccountForGroupHappyCaseTest() {

        //ARRANGE
        String groupID = "7001";
        String groupDescription = "family";
        String accountID = "7002";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String personID = "7001@switch.com";

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, accountDenomination, accountDescription);
        CreateAccountForGroupRequestDTO requestDTO = CreateAccountForGroupAssembler.mapToRequestDTO(accountID, groupID, accountDenomination, accountDescription, personID);
        CreateAccountForGroupResponseDTO expectedResponseDTO = new CreateAccountForGroupResponseDTO(groupID, groupDescription, accountID, accountDenomination);

        Mockito.when(service.createAccountForGroup(requestDTO)).thenReturn(expectedResponseDTO);

        //ACT
        Object actualResponseDTO = controller.createAccountForGroup(infoDTO, personID, groupID).getBody();

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);

    }

    /**
     * Method CreateAccountForGroup
     * group doesn't exist in the repository
     */
    @Test
    @DisplayName("create Account For group - group doesn't exist in the repository")
    public void createAccountForGroupRepositoryDoesNotContainGroupTest() {

        //ARRANGE
        String groupID = "7003";
        String accountID = "7005";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String personID = "7001@switch.com";

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, accountDenomination, accountDescription);
        CreateAccountForGroupRequestDTO requestDTO = CreateAccountForGroupAssembler.mapToRequestDTO(accountID, groupID, accountDenomination, accountDescription, personID);

        Mockito.when(service.createAccountForGroup(requestDTO)).thenThrow(GroupNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            controller.createAccountForGroup(infoDTO, personID, groupID);
        });
    }

    /**
     * CreateAccountForGroup
     * AccountID already exists in the acccount Repository
     */
    @Test
    @DisplayName("CreateAccountForGroup - AccountID already exists in the account repository")
    public void createAccountForGroupAccountAlreadyExistsInTheAccountRepository() {

        //ARRANGE
        String groupID = "7001";
        String accountID = "70010";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String personID = "7001@switch.com";

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, accountDenomination, accountDescription);
        CreateAccountForGroupRequestDTO requestDTO = CreateAccountForGroupAssembler.mapToRequestDTO(accountID, groupID, accountDenomination, accountDescription, personID);

        Mockito.when(service.createAccountForGroup(requestDTO)).thenThrow(AccountAlreadyExistsException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            controller.createAccountForGroup(infoDTO, personID, groupID);
        });
    }

    /**
     * CreateAccountForGroup
     * Person is not group's manager
     */
    @Test
    @DisplayName("CreateAccountForGroup - personID is not in group's list of managers")
    public void createAccountForGroupAccountPersonIsNotManagerOfTheGroupTest() {

        //ARRANGE
        String groupID = "7001";
        String accountID = "7001";
        String accountDenomination = "shopping";
        String accountDescription = "food";
        String personID = "7003@switch.com";

        CreateAccountForGroupInfoDTO infoDTO = new CreateAccountForGroupInfoDTO(accountID, accountDenomination, accountDescription);
        CreateAccountForGroupRequestDTO requestDTO = CreateAccountForGroupAssembler.mapToRequestDTO(accountID, groupID, accountDenomination, accountDescription, personID);

        Mockito.when(service.createAccountForGroup(requestDTO)).thenThrow(GroupConflictException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupConflictException.class, () -> {
            controller.createAccountForGroup(infoDTO, personID, groupID);
        });
    }

    /**
     * Test for getAccountByID method
     * Happy Case
     */
    @Test
    @DisplayName("CreateAccountForGroup - Happy Case")
    public void getAccountByIDHappyCaseTest() {
        //ARRANGE
        String accountID = "7001";
        String accountDenomination = "shopping";
        String accountDescription = "food";

        AccountDTO expectedResponseDTO = new AccountDTO(accountDenomination, accountDescription, accountID);

        Mockito.when(service.getAccountByID(accountID)).thenReturn(expectedResponseDTO);

        //ACT
        Object actualResponseDTO = controller.getAccountByID(accountID).getBody();

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Test for getAccountByID method
     * Ensure throws exception when account is not in account repo
     */
    @Test
    @DisplayName("CreateAccountForGroup - Ensure throws exception when account is not in account repo")
    public void getAccountByIDEnsureThrowsExceptionWhenAccountIsNotInRepoTest() {
        //ARRANGE
        String accountID = "7003";

        Mockito.when(service.getAccountByID(accountID)).thenThrow(AccountNotFoundException.class);

        //ASSERT
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            controller.getAccountByID(accountID);
        });
    }

    /**
     * Test for getAccountsByGroupID method
     * Happy Case
     */
    @Test
    @DisplayName("CreateAccountForGroup - personID is not in group's list of managers")
    public void getAccountsByGroupIDHappyCaseTest() {
        //ARRANGE
        String groupID = "7002";

        List<AccountDTO> accountsDTOList = new ArrayList<>();
        accountsDTOList.add(new AccountDTO("bla", "ble", "7001"));
        accountsDTOList.add(new AccountDTO("bla", "ble", "7002"));

        AccountsDTO expectedResponseDTO = new AccountsDTO(accountsDTOList);

        Mockito.when(service.getAccountsByGroupID(groupID)).thenReturn(expectedResponseDTO);
        //ACT
        Object actualResponseDTO = controller.getAccountsByGroupID(groupID).getBody();

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Test for getAccountsByGroupID method
     * Ensure throws exception if group doesn't exist in group repository
     */
    @Test
    @DisplayName("CreateAccountForGroup - Ensure throws exception if group doesn't exist in group repository")
    public void getAccountsByGroupIDEnsureExceptionIfGroupDoesNotExistTest() {
        //ARRANGE
        String groupID = "7001";

        Mockito.when(service.getAccountsByGroupID(groupID)).thenThrow(GroupNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            controller.getAccountsByGroupID(groupID);
        });
    }

}
