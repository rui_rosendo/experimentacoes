package project.controllers.web;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.*;
import project.exceptions.GroupAlreadyExistsException;
import project.exceptions.GroupLedgerAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSCreateGroupService;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class CreateGroupRESTControllerTest {

    @Autowired
    private IUSCreateGroupService service;

    @Autowired
    private CreateGroupRESTController controller;

    /**
     * Unit Test for createGroup method of CreateGroupRESTController class
     * Happy Case
     */
    @Test
    @DisplayName("Create Group - Happy Case")
    public void createGroupHappyCaseTest() {
        //Arrange
        String groupID = "21012";
        String description = "thisIsADescription";
        String creationDate = "2020-01-01";
        String creatorID = "21001@switch.com";
        String groupLedgerID = "21032";

        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        CreateGroupResponseDTO ExpectedDTO = new CreateGroupResponseDTO("21012", "thisIsADescription");
        CreateGroupRequestDTO requestDTO = CreateGroupAssembler.mapToRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);

        Mockito.when(service.createGroup(requestDTO)).thenReturn(ExpectedDTO);

        //Act
        Object actual = controller.createGroup(infoDTO, creatorID).getBody();

        //Assert
        assertEquals(ExpectedDTO, actual);

    }

    /**
     * Unit Test for createGroup method of CreateGroupRESTController class
     * Ensure exception message when group requested to be created already existed in group repository (by ID)
     */
    @Test
    @DisplayName("Ensure exception message when group requested to be created already existed in group repository (by ID)")
    public void createGroupEnsureExceptionWhenGroupIDAlreadyExistedTest() {
        //Arrange
        String groupID = "21012";
        String description = "thisIsADescription";
        String creationDate = "2020-01-01";
        String creatorID = "21001@switch.com";
        String groupLedgerID = "21032";

        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        CreateGroupRequestDTO requestDTO = CreateGroupAssembler.mapToRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);

        Mockito.when(service.createGroup(requestDTO)).thenThrow(GroupAlreadyExistsException.class);

        //Act
        //Assert
        Assertions.assertThrows(GroupAlreadyExistsException.class, () -> {
            controller.createGroup(infoDTO, creatorID);
        });

    }

    /**
     * Unit Test for createGroup method of CreateGroupRESTController class
     * Ensure exception message when Ledger requested to be created already existed in ledger repository (by ID)
     */
    @Test
    @DisplayName("Ensure exception message when Ledger requested to be created already existed in ledger repository (by ID)")
    public void createGroupEnsureErrorWhenGroupLedgerIDAlreadyExistedTest() {
        //Arrange
        String groupID = "21011";
        String description = "thisIsADescription";
        String creationDate = "2020-01-01";
        String creatorID = "21001@switch.com";
        String groupLedgerID = "21031";

        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);

        Mockito.when(service.createGroup(requestDTO)).thenThrow(GroupLedgerAlreadyExistsException.class);

        //Assert
        Assertions.assertThrows(GroupLedgerAlreadyExistsException.class, () -> {
            controller.createGroup(infoDTO, creatorID);
        });

    }

    /**
     * Unit Test for createGroup method of CreateGroupRESTController class
     * Ensure exception message when Creator already existed in person repository (by ID)
     */
    @Test
    @DisplayName("Ensure exception message when Creator already existed in person repository (by ID)")
    public void createGroupEnsureErrorWhenCreatorIDAlreadyExistedTest() {
        //Arrange
        String groupID = "21031";
        String description = "thisIsADescription";
        String creationDate = "2020-01-01";
        String creatorID = "21001@switch.com";
        String groupLedgerID = "21031";

        CreateGroupInfoDTO infoDTO = new CreateGroupInfoDTO(groupID, description, creationDate, groupLedgerID);
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);

        Mockito.when(service.createGroup(requestDTO)).thenThrow(PersonNotFoundException.class);

        //Act
        //Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            controller.createGroup(infoDTO, creatorID);
        });
    }

    /**
     * getGroupBy ID - Happy Case
     */
    @Test
    @DisplayName("getGroupByID - Happy Case - Unit Test Controller")
    public void getGroupByIDHappyCaseTest() {
        //Arrange
        String groupID = "21011";
        String description = "thisIsADescription";
        String creationDate = "2020-01-01";
        String creatorID = "21001@switch.com";
        String groupLedgerID = "21031";
        Set<String> members = new HashSet<>();
        members.add(creatorID);
        Set<String> managers = new HashSet<>();
        managers.add(creatorID);

        GroupDTO expected = new GroupDTO(members, managers, groupID, groupLedgerID, description, creationDate);

        Mockito.when(service.getGroupByID(groupID)).thenReturn(expected);

        //Act
        Object result = controller.getGroupByID(groupID).getBody();

        //Assert
        assertEquals(expected, result);
    }
}

