package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;
import project.dto.AddMemberInfoDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddMemberToGroupRESTControllerIT extends AbstractTest {
    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * Test for addMemberToGroup
     * Happy case
     */
    @Test
    @DisplayName("Test for addMember - HappyCase")
    public void addMemberToGroupHappyCaseTest() throws Exception {
        //Arrange
        String groupID = "4444";
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("groupID", groupID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/groups/{groupID}/members")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        String personID = "111@switch.pt";
        AddMemberInfoDTO newMemberInfoDTO = new AddMemberInfoDTO(personID);
        String inputJson = super.mapToJson(newMemberInfoDTO);

        int expectedStatus = 201;
        JSONObject expectedContent = new JSONObject()
                .put("newMemberEmail", personID)
                .put("groupID", groupID)
                .put("groupDescription", "Grupo da familia Alves")
                .put("_links", new JSONObject()
                        .put("members", new JSONObject().
                                put("href", "http://localhost/groups/" + groupID + "/members")));

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Test for addMemberToGroup
     * Ensure exception is thrown when no Person with given PersonID is found in repository case
     */
    @Test
    @DisplayName("Test for addMember - No person with given PersonID case")
    public void addMemberToGroupNoPersonWithGivenIDCaseTest() throws Exception {
        //Arrange
        String groupID = "4444";
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("groupID", groupID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/groups/{groupID}/members")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        String personID = "999@switch.pt";
        AddMemberInfoDTO newMemberInfoDTO = new AddMemberInfoDTO(personID);
        String inputJson = super.mapToJson(newMemberInfoDTO);

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Person not found")
                .put("errors", new JSONArray(Collections.singletonList("Person not found")));

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Test for addMemberToGroup
     * Ensure exception is thrown when no Group with given GroupID is found in repository case
     */
    @Test
    @DisplayName("Test for addMember - No group with given GroupID case")
    public void addMemberToGroupNoGroupWithGivenIDCaseTest() throws Exception {
        //Arrange
        String groupID = "666";
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("groupID", groupID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/groups/{groupID}/members")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        String personID = "111@switch.pt";
        AddMemberInfoDTO newMemberInfoDTO = new AddMemberInfoDTO(personID);
        String inputJson = super.mapToJson(newMemberInfoDTO);

        int expectedStatus = 404;
        JSONObject expectedContent = new JSONObject()
                .put("status", "NOT_FOUND")
                .put("message", "Group not found")
                .put("errors", new JSONArray(Collections.singletonList("Group not found")));

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

    /**
     * Test for getMembersByGroupID
     * Happy case
     */
    @Test
    @DisplayName("Test for getMembersByGroupID - HappyCase")
    public void getMembersByGroupIDHappyCaseTest() throws Exception {
        //Arrange
        String groupID = "4444";
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("groupID", groupID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/groups/{groupID}/members")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 200;
        JSONObject expectedContent = new JSONObject()
                .put("membersIDs", new JSONArray(Arrays.asList("333@switch.pt", "1996@switch.pt", "222@switch.pt")))
                .put("names", new JSONArray(Arrays.asList("AndrÃ©", "Diogo", "JoÃ£o")))
                .put("isManager", new JSONArray(Arrays.asList(true, false, false)))
                .put("_links", new JSONObject()
                        .put("member 1996@switch.pt", new JSONObject()
                                .put("href", "http://localhost/" + groupID + "/member/1996@switch.pt"))
                        .put("member 222@switch.pt", new JSONObject()
                                .put("href", "http://localhost/" + groupID + "/member/222@switch.pt"))
                        .put("member 333@switch.pt", new JSONObject()
                                .put("href", "http://localhost/" + groupID + "/member/333@switch.pt")));

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());
        System.out.println(actualContent);

        //Assert
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }
}
