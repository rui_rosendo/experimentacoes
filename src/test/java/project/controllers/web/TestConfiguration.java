package project.controllers.web;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import project.application.services.*;
import project.dto.PersonsResponseDTO;

@Profile("test")
@Configuration
public class TestConfiguration {
    @Bean
    @Primary
    public CheckIfPeopleAreSiblingsService CheckIfPeopleAreSiblingsService() {
        return Mockito.mock(CheckIfPeopleAreSiblingsService.class);
    }

    @Bean
    @Primary
    public AddMemberToGroupService AddMemberToGroupService() {
        return Mockito.mock(AddMemberToGroupService.class);
    }

    @Bean
    @Primary
    public CreateAccountForPersonService CreateAccountForPersonService() {
        return Mockito.mock(CreateAccountForPersonService.class);
    }

    @Bean
    @Primary
    public CreateAccountForGroupService CreateAccountForGroupService() {
        return Mockito.mock(CreateAccountForGroupService.class);
    }

    @Bean
    @Primary
    public GetFamilyGroupsService GetFamilyGroupsService() {
        return Mockito.mock(GetFamilyGroupsService.class);
    }

    @Bean
    @Primary
    public GetPersonsGroupsService GetPersonsGroupsService() {
        return Mockito.mock(GetPersonsGroupsService.class);
    }

    @Bean
    @Primary
    public CreateGroupService CreateGroupService() {
        return Mockito.mock(CreateGroupService.class);
    }

    @Bean
    @Primary
    public CreateCategoryForGroupService CreateCategoryForGroupService() {
        return Mockito.mock(CreateCategoryForGroupService.class);
    }

    @Bean
    @Primary
    public CreateCategoryForPersonService CreateCategoryForPersonService() {
        return Mockito.mock(CreateCategoryForPersonService.class);
    }

    @Bean
    @Primary
    public CreateTransactionForGroupService CreateTransactionForGroupService() {
        return Mockito.mock(CreateTransactionForGroupService.class);
    }

    @Bean
    @Primary
    public GetTransactionsService GetTransactionsService() {
        return Mockito.mock(GetTransactionsService.class);
    }

    @Bean
    @Primary
    public GetCategoriesService GetCategoriesService() {
        return Mockito.mock(GetCategoriesService.class);
    }

    @Bean
    @Primary
    public FilterPersonByNameService FilterPersonByNameService() {
        return Mockito.mock(FilterPersonByNameService.class);
    }


}
