//package project.controllers;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.ledger.Transaction;
//import project.model.domainentities.ledger.Type;
//import project.model.domainentities.person.Person;
//import project.model.domainentities.shared.Category;
//import project.model.domainentities.shared.Period;
//
//import java.util.HashSet;
//import java.util.Set;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class PersonTransactionsBetweenDatesControllerTest {
//
//    private PersonTransactionsWithinPeriodController control;
//    private Person goncalo;
//    private Category c1;
//    private Category c2;
//    private Account debit;
//    private Account debit2;
//    private Account credit;
//    private Account credit2;
//
//    @BeforeEach
//    public void init() {
//
//        control = new PersonTransactionsWithinPeriodController();
//        goncalo = new Person("Goncalo", "Rua Abilo Beça", "Bragança", "1989-07-31", null, null);
//
//        c1 = new Category("health");
//        c2 = new Category("medication");
//
//        debit = new Account("pills", "pay pills");
//        credit = new Account("pharmacy", "pharmacy from main street");
//        debit2 = new Account("health", "pay medical expenses");
//        credit2 = new Account("hospital", "hospital bills");
//
//        goncalo.addCategory("health");
//        goncalo.addCategory("medication");
//        goncalo.addAccount("pills", "pay pills");
//        goncalo.addAccount("health", "pay medical expenses");
//        goncalo.addAccount("hospital", "hospital bills");
//        goncalo.addAccount("pharmacy", "pharmacy from main street");
//    }
//
//    /**
//     * Test for PersonTransactionsBetweenDatesControllerTest - HappyCase
//     */
//    @Test
//    @DisplayName("Test for PersonTransactionsBetweenDatesControllerTest - HappyCase")
//    void personTransactionsBetweenDatesControllerTestHappyCase() {
//        //ARRANGE
//
//        Transaction one = new Transaction(20.50, Type.DEBIT, "2020-02-25 00:00:00", "health", c1, debit, credit);
//        goncalo.addTransaction(20.50, Type.DEBIT, "2020-02-25 00:00:00", "health", c1, debit, credit);
//        goncalo.addTransaction(30.50, Type.DEBIT, "2021-03-25 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        transactions.add(one);
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getPersonTransactionsWithinPeriod(goncalo, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//
//    }
//
//    /**
//     * Test for PersonTransactionsBetweenDatesControllerTest - Only one in the period
//     */
//    @Test
//    @DisplayName("Test for PersonTransactionsBetweenDatesControllerTest - Only one in the period")
//    void personTransactionsBetweenDatesControllerTest() {
//        //ARRANGE
//
//        Transaction one = new Transaction(20.50, Type.DEBIT, "2020-02-25 00:00:00", "health", c1, debit, credit);
//        goncalo.addTransaction(20.50, Type.DEBIT, "2020-02-25 00:00:00", "health", c1, debit, credit);
//        goncalo.addTransaction(30.50, Type.DEBIT, "2021-03-25 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        transactions.add(one);
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getPersonTransactionsWithinPeriod(goncalo, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//
//    }
//
//    /**
//     * Test for PersonTransactionsBetweenDatesControllerTest - Empty
//     */
//    @Test
//    @DisplayName("Test for PersonTransactionsBetweenDatesControllerTest - Empty")
//    void personTransactionsOutsidePeriodTest() {
//        //ARRANGE
//
//        goncalo.addTransaction(20.50, Type.DEBIT, "2019-02-25 00:00:00", "health", c1, debit, credit);
//        goncalo.addTransaction(30.50, Type.DEBIT, "2021-03-25 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getPersonTransactionsWithinPeriod(goncalo, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//
//    }
//
//    /**
//     * Test for PersonTransactionsBetweenDatesControllerTest - limit superior
//     */
//    @Test
//    @DisplayName("Test for PersonTransactionsBetweenDatesControllerTest - limit superior")
//    void personTransactionsPeriodSuperiorLimitTest() {
//        //ARRANGE
//
//        Transaction one = new Transaction(30.50, Type.DEBIT, "2020-12-31 00:00:00", "gym", c2, debit2, credit2);
//        goncalo.addTransaction(20.50, Type.DEBIT, "2019-02-25 00:00:00", "health", c1, debit, credit);
//        goncalo.addTransaction(30.50, Type.DEBIT, "2020-12-31 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        transactions.add(one);
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getPersonTransactionsWithinPeriod(goncalo, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//
//    }
//
//    /**
//     * Test for PersonTransactionsBetweenDatesControllerTest - limit inferior
//     */
//    @Test
//    @DisplayName("Test for PersonTransactionsBetweenDatesControllerTest - limit inferior")
//    void personTransactionsPeriodInferiorLimitTest() {
//        //ARRANGE
//
//        Transaction one = new Transaction(55.50, Type.DEBIT, "2020-01-01 00:00:00", "gym", c2, debit2, credit2);
//        goncalo.addTransaction(20.50, Type.DEBIT, "2019-02-25 00:00:00", "health", c1, debit, credit);
//        goncalo.addTransaction(55.50, Type.DEBIT, "2020-01-01 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        transactions.add(one);
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getPersonTransactionsWithinPeriod(goncalo, period);
//
//        //ASSERT
//        assertEquals(expected, result);
//
//    }
//
//    /**
//     * Test for PersonTransactionsBetweenDatesControllerTest - NotEquals Test
//     */
//    @Test
//    @DisplayName("Test for PersonTransactionsBetweenDatesControllerTest - NotEqualsTest")
//    void personTransactionsPeriodNotEqualsTest() {
//        //ARRANGE
//
//        goncalo.addTransaction(20.50, Type.DEBIT, "2019-02-25 00:00:00", "health", c1, debit, credit);
//        goncalo.addTransaction(50.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//        Period period = new Period("2020-01-01 00:00:00", "2020-12-31 00:00:00");
//        Set<Transaction> transactions = new HashSet<>();
//        String expected = transactions.toString();
//
//        //ACT
//        String result = control.getPersonTransactionsWithinPeriod(goncalo, period);
//
//        //ASSERT
//        assertNotEquals(expected, result);
//
//    }
//
//    /**
//     * Exception with amount = 0
//     */
//    @Test
//    @DisplayName("Ensure IllegalArgumentException with amount = 0")
//    void personTransactionsPeriodEnsureExceptionWithAmountZeroTest() {
//
//        // Act
//        // Assert
//        assertThrows(IllegalArgumentException.class, () -> {
//            goncalo.addTransaction(0, Type.DEBIT, "2020-01-08 00:00:00", "Groceries for January", c1, debit, credit);
//        });
//    }
//
//    /**
//     * Exception with amount = 0
//     */
//    @Test
//    @DisplayName("Ensure IllegalArgumentException category not added")
//    void personTransactionsPeriodEnsureExceptionWithCategoryNotAddedTest() {
//
//        // Arrange
//        Category c3 = new Category("food");
//        // Act
//        // Assert
//        assertThrows(IllegalArgumentException.class, () -> {
//            goncalo.addTransaction(10, Type.DEBIT, "2020-01-08 00:00:00", "Groceries for January", c3, debit, credit);
//        });
//    }
//
//    /**
//     * Exception with amount = 0
//     */
//    @Test
//    @DisplayName("Ensure IllegalArgumentException account debit not added")
//    void personTransactionsPeriodEnsureExceptionWithAccountDebitNotAddedTest() {
//
//        // Arrange
//        Account debit3 = new Account("debit3", "debit3");
//        // Act
//        // Assert
//        assertThrows(IllegalArgumentException.class, () -> {
//            goncalo.addTransaction(10, Type.DEBIT, "2020-01-08 00:00:00", "Groceries for January", c1, debit3, credit2);
//        });
//    }
//
//    /**
//     * Exception with amount = 0
//     */
//    @Test
//    @DisplayName("Ensure IllegalArgumentException account credit not added")
//    void personTransactionsPeriodEnsureExceptionWithAccountCreditNotAddedTest() {
//
//        // Arrange
//        Account credit3 = new Account("credit3", "credit3");
//        // Act
//        // Assert
//        assertThrows(IllegalArgumentException.class, () -> {
//            goncalo.addTransaction(10, Type.DEBIT, "2020-01-08 00:00:00", "Groceries for January", c1, debit, credit3);
//        });
//    }
//
//}
