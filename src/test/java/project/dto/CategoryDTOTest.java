package project.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CategoryDTOTest {

    /**
     * Test for CategoryDTO Constructor
     */
    @Test
    void constructorTest() {
        //ARRANGE
        String expected = "school";
        CategoryDTO dto = new CategoryDTO(expected);

        //ASSERT
        assertTrue(dto instanceof CategoryDTO);
    }

    /**
     * Test for getDesignation method
     * Happy Case
     */
    @Test
    void getDesignationHappyCaseTest() {
        //ARRANGE
        String expected = "school";
        CategoryDTO dto = new CategoryDTO(expected);

        //ACT
        String actual = dto.getDesignation();

        //ASSERT
        assertEquals(expected, actual);

    }

    @Test
    void testHashCode() {
        //ARRANGE
        CategoryDTO dto = new CategoryDTO("school");

        int expected = -907977809;

        //ACTUAL
        int actual = dto.hashCode();

        //ASSERT
        assertEquals(expected, actual);

    }

    @Test
    void testToString() {
        CategoryDTO dto = new CategoryDTO("school");

        String expected = "CategoryDTO{designation='school'}";
        //ACTUAL
        String actual = dto.toString();

        //ASSERT
        assertEquals(expected, actual);
    }
}