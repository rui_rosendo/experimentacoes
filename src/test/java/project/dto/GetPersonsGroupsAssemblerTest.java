package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GetPersonsGroupsAssemblerTest {

    Set<Group> groups;

    @BeforeEach
    public void init() {
        PersonID personID = new PersonID("1@switch.pt");
        GroupID groupID1 = new GroupID("1");
        LedgerID ledgerID = new LedgerID("1");
        Group groupA = new Group(groupID1, "general", "2019-12-18", personID, ledgerID);
        Group groupB = new Group(groupID1, "more general", "2020-12-18", personID, ledgerID);

        groups = new HashSet<>();
        groups.add(groupA);
        groups.add(groupB);

    }

    /**
     * Test for GetPersonsGroupsAssembler Constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for GetPersonsGroupsAssembler Constructor - Happy Case")
    void instanceOfGetPersonsGroupsAssembler() {
        GetPersonsGroupsAssembler assembler = new GetPersonsGroupsAssembler();
        assertTrue(assembler instanceof GetPersonsGroupsAssembler);
    }

    /**
     * Test for mapToDTO Method
     * Happy case
     */
    @Test
    void mapToDTOHappyCase() {
        //Arrange
        List<GroupDTOMinimal> groupDTOs = new ArrayList<>();

        for (Group group : groups) {
            groupDTOs.add(new GroupDTOMinimal(group.getID().toStringDTO(), group.getDescription().getDescriptionValue(), group.getCreationDate().toStringDTO()));
        }

        GetPersonsGroupsResponseDTO expected = new GetPersonsGroupsResponseDTO(groupDTOs);

        //Act
        GetPersonsGroupsResponseDTO result = GetPersonsGroupsAssembler.mapToDTO(groups);

        //Assert
        assertEquals(expected, result);
    }

}