package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CreateAccountForPersonAssemblerTest {

    private PersonID newPersonID;
    private LedgerID newLedgerID;
    private AccountID newAccountID;
    private PersonID newMotherID;
    private PersonID newFatherID;
    private AccountID otherAccountID;

    @BeforeEach
    public void initialize() {
        newPersonID = new PersonID("1@switch.pt");
        newLedgerID = new LedgerID("1");
        newAccountID = new AccountID("1");
        newMotherID = new PersonID("1@switch.pt");
        newFatherID = new PersonID("1@switch.pt");
        otherAccountID = new AccountID("2");

    }

    /**
     * Test for mapToDTORequest Method
     * Happy case
     */
    @Test
    void mapToDTORequestHappyCase() {

        //ARRANGE
        CreateAccountForPersonRequestDTO expected = new CreateAccountForPersonRequestDTO("2", "1@switch.pt", "food", "food");
        //ACT
        CreateAccountForPersonRequestDTO result = CreateAccountForPersonAssembler.mapToRequestDTO("2", "1@switch.pt", "food", "food");
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToDTOResponse Method
     * Happy case
     */
    @Test
    void mapToDTOResponseHappyCase() {

        //ARRANGE
        CreateAccountForPersonResponseDTO expected = new CreateAccountForPersonResponseDTO("2@switch.pt", "Manuel", "1", "food");
        //ACT
        CreateAccountForPersonResponseDTO result = CreateAccountForPersonAssembler.mapToResponseDTO("2@switch.pt", "Manuel", "1", "food");
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for CreateAccountForPersonAssembler Constructor - Happy Case
     */
    @Test
    @DisplayName("Test for CreateAccountForPersonAssembler Constructor - Happy Case")
    void instanceOf() {
        CreateAccountForPersonAssembler assembler = new CreateAccountForPersonAssembler();
        assertTrue(assembler instanceof CreateAccountForPersonAssembler);
    }

}