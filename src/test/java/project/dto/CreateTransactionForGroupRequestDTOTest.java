package project.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateTransactionForGroupRequestDTOTest {

    /**
     * Test for CreateTransactionForGroupResponseDTO constructor
     */
    @Test
    void constructorTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        //Act
        //Assert
        assertTrue(requestDTO instanceof CreateTransactionForGroupRequestDTO);
    }

    /**
     * Test for getPersonID method
     * Happy Case
     */
    @Test
    void getPersonIDHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "100";

        //Act
        String actual = requestDTO.getPersonEmail();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getPersonID method
     * Ensure not equals
     */
    @Test
    void getPersonIDEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "999";

        //Act
        String actual = requestDTO.getPersonEmail();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getGroupID method
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "200";

        //Act
        String actual = requestDTO.getGroupID();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getGroupID method
     * Ensure not equals
     */
    @Test
    void getGroupIDEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "100";

        //Act
        String actual = requestDTO.getGroupID();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getAmount method
     * Happy Case
     */
    @Test
    void getAmountHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1000";

        //Act
        String actual = requestDTO.getAmount();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getAmount method
     * Ensure not equals
     */
    @Test
    void getAmountEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "9999";

        //Act
        String actual = requestDTO.getAmount();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getDateTime method
     * Happy Case
     */
    @Test
    void getDateTimeHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "2000-01-01 00 00 00";

        //Act
        String actual = requestDTO.getDateTime();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getDateTime method
     * Ensure not equals
     */
    @Test
    void getDateTimeEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1900-01-01 00 00 00";

        //Act
        String actual = requestDTO.getDateTime();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Happy Case
     */
    @Test
    void getTypeHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1";

        //Act
        String actual = requestDTO.getType();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Ensure not equals
     */
    @Test
    void getTypeEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "-1";

        //Act
        String actual = requestDTO.getType();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getDescription method
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "description";

        //Act
        String actual = requestDTO.getDescription();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getDescription method
     * Ensure not equals
     */
    @Test
    void getDescriptionEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "different description";

        //Act
        String actual = requestDTO.getDescription();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getCategoryDesignation method
     * Happy Case
     */
    @Test
    void getCategoryDesignationHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "categoryDesignation";

        //Act
        String actual = requestDTO.getCategoryDesignation();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getCategoryDesignation method
     * Ensure not equals
     */
    @Test
    void getCategoryDesignationEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "different categoryDesignation";

        //Act
        String actual = requestDTO.getCategoryDesignation();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getDebitAccountID method
     * Happy Case
     */
    @Test
    void getDebitAccountIDHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1000";

        //Act
        String actual = requestDTO.getDebitAccountID();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getDebitAccountID method
     * Ensure not equals
     */
    @Test
    void getDebitAccountIDEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "9999";

        //Act
        String actual = requestDTO.getDebitAccountID();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getCreditAccountID method
     * Happy Case
     */
    @Test
    void getCreditAccountIDHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "2000";

        //Act
        String actual = requestDTO.getCreditAccountID();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getCreditAccountID method
     * Ensure not equals
     */
    @Test
    void getCreditAccountIDEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1000";

        //Act
        String actual = requestDTO.getCreditAccountID();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for equals method
     * Ensure true when comparing the same object
     */
    @Test
    void testEqualsEnsureTrueWhenComparingSameObjectTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        //Act
        boolean actual = requestDTO.equals(requestDTO);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for equals method
     * Ensure false when other object is null
     */
    @Test
    void testEqualsEnsureFalseWhenOtherObjectIsNullTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        CreateTransactionForGroupRequestDTO otherRequestDTO = null;

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when other object is from different class
     */
    @Test
    void testEqualsEnsureFalseWhenOtherObjectIsFromDifferentClassTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String otherRequestDTO = "other";

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure true when all attributes are equals
     */
    @Test
    void testEqualsEnsureTrueWhenAllAttributesAreEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        CreateTransactionForGroupRequestDTO otherRequestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for equals method
     * Ensure false when personID is different
     */
    @Test
    void testEqualsEnsureFalseWhenPersonIDIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(differentAttribute, groupID,
                        amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when groupID is different
     */
    @Test
    void testEqualsEnsureFalseWhenGroupIDIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(personID, differentAttribute,
                        amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when amount is different
     */
    @Test
    void testEqualsEnsureFalseWhenAmountIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(personID, groupID,
                        differentAttribute, dateTime, type, description, categoryDesignation, debitAccountID,
                        creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when dateTime is different
     */
    @Test
    void testEqualsEnsureFalseWhenDateTimeIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(personID, groupID,
                        amount, differentAttribute, type, description, categoryDesignation, debitAccountID,
                        creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when type is different
     */
    @Test
    void testEqualsEnsureFalseWhenTypeIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(personID, groupID,
                        amount, dateTime, differentAttribute, description, categoryDesignation, debitAccountID,
                        creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when description is different
     */
    @Test
    void testEqualsEnsureFalseWhenDescriptionIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(personID, groupID,
                        amount, dateTime, type, differentAttribute, categoryDesignation, debitAccountID,
                        creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when categoryDesignation is different
     */
    @Test
    void testEqualsEnsureFalseWhenCategoryDesignationIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(personID, groupID,
                        amount, dateTime, type, description, differentAttribute, debitAccountID,
                        creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when debitAccountID is different
     */
    @Test
    void testEqualsEnsureFalseWhenDebitAccountIDIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(personID, groupID,
                        amount, dateTime, type, description, categoryDesignation, differentAttribute,
                        creditAccountID);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when creditAccountID is different
     */
    @Test
    void testEqualsEnsureFalseWhenCreditAccountIDIsDifferentTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupRequestDTO otherRequestDTO =
                new CreateTransactionForGroupRequestDTO(personID, groupID,
                        amount, dateTime, type, description, categoryDesignation, debitAccountID,
                        differentAttribute);

        //Act
        boolean actual = requestDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for hashCode method
     */
    @Test
    void testHashCode() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personID, groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        int expected = 1174991464;

        //Act
        int actual = requestDTO.hashCode();

        //Assert
        assertEquals(expected, actual);
    }
}