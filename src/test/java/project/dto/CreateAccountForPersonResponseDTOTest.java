package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import static org.junit.jupiter.api.Assertions.*;

class CreateAccountForPersonResponseDTOTest {
    /**
     * Test for CreateAccountForPersonResponseDTO constructor
     */
    @Test
    @DisplayName("Test for CreateAccountForPersonDTO constructor")
    void constructorHappyCaseTest() {
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        //ASSERT
        assertTrue(dto instanceof CreateAccountForPersonResponseDTO);
    }

    /**
     * Test for getPersonID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getPersonID - Happy Case")
    void getPersonIDHappyCaseTest() {

        //ARRANGE
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        String expected = "1";
        //ACT
        String result = dto.getPersonEmail();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getPersonName
     * Happy case
     */
    @Test
    @DisplayName(" Test for getPersonName - Happy Case")
    void getPersonNameHappyCaseTest() {

        //ARRANGE
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","2","food");
        String expected = "Manuel";
        //ACT
        String result = dto.getPersonName();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getAccountID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getAccountID- Happy Case")
    void getAccountIDHappyCaseTest() {

        //ARRANGE
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        String expected = "1";
        //ACT
        String result = dto.getAccountID();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getAccountDenomination
     * Happy case
     */
    @Test
    @DisplayName(" Test for getAccountDenomination Happy Case")
    void getAccountDenominationHappyCaseTest() {

        //ARRANGE
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        String expected = "food";
        //ACT
        String result = dto.getAccountDenomination();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto1 = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        CreateAccountForPersonResponseDTO dto2 = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Not Equals - Different PersonID
     */
    @Test
    @DisplayName("Test for equals - Different PersonID")
    void equalsDifferentPersonIDTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto1 = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        CreateAccountForPersonResponseDTO dto2 = new CreateAccountForPersonResponseDTO("2","Manuel","1","food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Not Equals - Different PersonName
     */
    @Test
    @DisplayName("Test for equals - Different PersonName")
    void equalsDifferentPersonNameTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto1 = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        CreateAccountForPersonResponseDTO dto2 = new CreateAccountForPersonResponseDTO("1","Luis","1","food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Not Equals - Different AccountID
     */
    @Test
    @DisplayName("Test for equals - Different AccountID")
    void equalsDifferentAccountIDTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto1 = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        CreateAccountForPersonResponseDTO dto2 = new CreateAccountForPersonResponseDTO("1","Manuel","2","food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Not Equals - Different AccountDenomination
     */
    @Test
    @DisplayName("Test for equals - Different AccountDenomination")
    void equalsDifferentAccountDenominationTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto1 = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        CreateAccountForPersonResponseDTO dto2 = new CreateAccountForPersonResponseDTO("1","Manuel","1","comida");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        CreateAccountForPersonResponseDTO dto2 = null;
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with diferent object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        GroupID groupID = new GroupID("1");
        //Act
        boolean result = dto.equals(groupID);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        //Act
        boolean result = dto.equals(dto);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        CreateAccountForPersonResponseDTO dto = new CreateAccountForPersonResponseDTO("1","Manuel","1","food");
        int expected = 211858399;
        //Act
        int result = dto.hashCode();
        //Assert
        assertEquals(expected, result);
    }
}