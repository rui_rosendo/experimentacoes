package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PersonAssemblerTest {
    /**
     * Test for PersonAssemblerTest Constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for PersonAssemblerTest Constructor - Happy Case")
    void instanceOfPersonAssemblerTest() {
        PersonAssembler assembler = new PersonAssembler();
        assertTrue(assembler instanceof PersonAssembler);
    }

    /**
     * Test for mapToResponseDTO Method
     * Happy case
     */
    @Test
    void mapToResponseDTOHappyCase() {
        //Arrange
        GetPersonInfoResponseDTO expected = new GetPersonInfoResponseDTO("21001@switch.pt", "João",
                "Travessa Santa Bárbara", "1987-04-17", "Estarreja");

        //Act
        GetPersonInfoResponseDTO result = PersonAssembler.mapToResponseDTO("21001@switch.pt", "João",
                "Travessa Santa Bárbara", "1987-04-17", "Estarreja");

        //Assert
        assertEquals(expected, result);
    }
}