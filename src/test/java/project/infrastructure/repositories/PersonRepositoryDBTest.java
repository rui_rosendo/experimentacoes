package project.infrastructure.repositories;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.PersonRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Transactional
class PersonRepositoryDBTest {

    @Autowired
    PersonRepository repository;

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for PersonRepository constructor - HappyCase")
    void constructorHappyCaseTest() {
        assertTrue(repository instanceof PersonRepository);
    }

    /**
     * Test for save
     * Happy case
     */
    @Test
    @DisplayName("Test for PersonRepository save - HappyCase")
    void saveHappyCaseTest() {
        // Arrange
        PersonID personID = new PersonID("ajms@mail.com");
        Person expected = new Person(
                personID,
                "Antonio",
                "Rua das Valongas",
                "Porto",
                "1991-12-22",
                null,
                null,
                new LedgerID("1")
        );

        // Act
        Person actual = repository.save(expected);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for findById
     * Happy case
     */
    @Test
    @DisplayName("Test for findById - HappyCase")
    void findByIdHappyCaseTest() {
        // Arrange
        PersonID personID = new PersonID("ajms@mail.com");
        Person person = new Person(
                personID,
                "Antonio",
                "Rua das Valongas",
                "Porto",
                "1991-12-22",
                null,
                null,
                new LedgerID("1")
        );
        repository.save(person);

        Optional<Person> expected = Optional.of(person);

        // Act
        Optional<Person> actual = repository.findById(personID);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for findById
     * Ensure works with null test
     */
    @Test
    @DisplayName("Test for findById - Ensure works with null test")
    void findByIdEnsureWorksWithNullTest() {
        // Arrange
        Optional<Person> expected = Optional.empty();

        // Act
        Optional<Person> actual = repository.findById(null);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test for filterByName - Ensure works with results")
    void filterByNameWithResultsTest() {
        //Arrange
        Person maria = new Person(new PersonID("4004@switch.pt"), "maria", "Rua Santa Catarina 35", "Porto", "1989-12-18", new PersonID("4001@switch.pt"), new PersonID("4002@switch.pt"), new LedgerID("4004"));
        List<Person> expected = new ArrayList<>();
        expected.add(maria);
        //Act
        List<Person> result = repository.filterByName("ria");

        //Assert
        assertEquals(expected,result);
    }

    @Test
    @DisplayName("Test for filterByName - Ensure works with no results")
    void filterByNameWithNoResultsTest() {
        //Arrange
        List<Person> expected = new ArrayList<>();
        //Act
        List<Person> result = repository.filterByName("abc");

        //Assert
        assertEquals(expected,result);
    }


}