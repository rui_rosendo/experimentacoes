package project.model.entities.group;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.AccountID;

import static org.junit.jupiter.api.Assertions.*;

class GroupIDTest {

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void constructorHappyCaseTest() {
        // Arrange
        GroupID one = new GroupID("1");

        // Act
        // Assert
        assertTrue(one instanceof GroupID);
    }

    /**
     * Test for sameValueAs
     * Ensure true
     */
    @DisplayName("Test for sameValueAs - Ensure true")
    @Test
    void sameValueAsEnsureTrueTest() {
        // Arrange
        GroupID a = new GroupID("1");
        GroupID b = new GroupID("1");

        // Act
        boolean result = a.sameValueAs(b);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for sameValueAs
     * Ensure false with null
     */
    @DisplayName("Test for Group sameValueAs - True case")
    @Test
    void sameValueAsEnsureFalseWithNullTest() {
        // Arrange
        GroupID a = new GroupID("1");

        // Act
        boolean result = a.sameValueAs(null);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for sameValueAs
     * Ensure false with different ID
     */
    @DisplayName("Test for sameValueAs - Ensure false with different ID")
    @Test
    void sameValueAsEnsureFalseWithDifferentIDTest() {
        // Arrange
        GroupID a = new GroupID("1");
        GroupID b = new GroupID("2");

        // Act
        boolean result = a.sameValueAs(b);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @DisplayName("Test for hashcode - Ensure works")
    @Test
    void hashCodeEnsureWorksTest() {
        // Arrange
        GroupID a = new GroupID("1");
        int expected = 32;

        // Act
        int result = a.hashCode();

        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * Ensure true with same object
     */
    @DisplayName("Test for equals - Ensure true with same object")
    @Test
    void equalsEnsureTrueWithSameObjectTest() {
        // Arrange
        GroupID a = new GroupID("1");

        // Act
        boolean result = a.equals(a);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with object from different class
     */
    @DisplayName("Test for equals - Ensure false with object from different class")
    @Test
    void equalsEnsureFalseWithObjectFromDifferentClassTest() {
        // Arrange
        GroupID a = new GroupID("1");
        AccountID b = new AccountID("1");

        // Act
        boolean result = a.equals(b);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true
     */
    @DisplayName("Test for equals - Ensure true")
    @Test
    void equalsEnsureTrueTest() {
        // Arrange
        GroupID a = new GroupID("1");
        GroupID b = new GroupID("1");

        // Act
        boolean result = a.equals(b);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for GroupID toString
     * Happy Case
     */
    @Test
    @DisplayName("Test for toString - Happy Case")
    void toStringHappyCaseTest() {
        GroupID newGroupID = new GroupID("213254123");
        String expected = "GroupID{groupID=213254123}";

        String result = newGroupID.toString();

        assertEquals(expected, result);
    }

    /**
     * Test for toString - not equals case
     */
    @Test
    @DisplayName("test for toString - not equals case")
    void toStringNotEqualsTest() {
        //Arrange
        GroupID groupID = new GroupID("1");
        String expected = "";
        //Act
        String result = groupID.toString();
        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for GroupID toStringDTO
     * Happy Case
     */
    @Test
    @DisplayName("Test for toStringDTO - Happy Case")
    void toStringDTOHappyCaseTest() {
        GroupID newGroupID = new GroupID("100");
        String expected = "100";

        String result = newGroupID.toStringDTO();

        assertEquals(expected, result);
    }

    /**
     * Test for toStringDTO - not equals case
     */
    @Test
    @DisplayName("test for toStringDTO - not equals case")
    void toStringDTONotEqualsTest() {
        //Arrange
        GroupID groupID = new GroupID("1");
        String expected = "";
        //Act
        String result = groupID.toStringDTO();
        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getId - Happy Case
     */
    @Test
    @DisplayName("test for getId")
    void getIdHappyCaseTest() {
        //ARRANGE
        GroupID a = new GroupID("1");
        Long expected = 1L;
        //ACT
        Long result = a.getId();
        //ASSERT
        assertEquals(expected, result);
    }
}