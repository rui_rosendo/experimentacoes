package project.model.entities.person;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.person.Birthplace;
import project.model.entities.shared.Designation;

import static org.junit.jupiter.api.Assertions.*;

class BirthplaceTest {

    /**
     * Test for Birthplace Constructor
     * Happy case (valid BirthPlace)
     */
    @Test
    @DisplayName("Test for Birthplace constructor - Happy case")
    void BirthplaceConstructorHappyCaseTest() {
        Birthplace porto = new Birthplace("Porto");
        assertTrue(porto instanceof Birthplace);
    }

    /**
     * Test for Birthplace Constructor
     * Exception with invalid BirthPlace - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with null BirthPlace")
    void BirthplaceConstructorEnsureExceptionWithNullTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Birthplace porto = new Birthplace(null);
        });
    }

    /**
     * Test for Birthplace Constructor
     * Exception with invalid BirthPlace - empty String
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with empty BirthPlace")
    void BirthplaceConstructorEnsureExceptionWithEmptyTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Birthplace porto = new Birthplace("");
        });
    }

    /**
     * Test for Birthplace Constructor
     * Exception with invalid BirthPlace - only whitespaces
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with only whitespaces BirthPlace")
    void BirthplaceConstructorEnsureExceptionWithOnlyWhitespacesTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Birthplace porto = new Birthplace("     ");
        });
    }

    /**
     * Test for Birthplace equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same Object")
    void BirthplaceEqualsSameObjectTest() {
        //Arrange
        Birthplace porto = new Birthplace("Porto");
        //Act
        boolean result = porto.equals(porto);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for Birthplace equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - Ensure true with objects with same attribute")
    void BirthplaceEqualsSameObjectsTest() {
        //Arrange
        Birthplace porto = new Birthplace("Porto");
        Birthplace otherPorto = new Birthplace("Porto");
        //Act
        boolean result = porto.equals(otherPorto);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for Birthplace equals
     * Null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null object")
    void BirthplaceNullTest() {
        //Arrange
        Birthplace porto = new Birthplace("Porto");
        Birthplace nullPlace = null;
        //Act
        boolean result = porto.equals(nullPlace);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for Birthplace equals
     * Null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with objects from different classes")
    void BirthplaceObjectsDifferentClassTest() {
        //Arrange
        Birthplace porto = new Birthplace("Porto");
        Designation food = new Designation("Pizza");
        //Act
        boolean result = porto.equals(food);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for Birthplace hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Happy Case")
    void BirthplaceHashCodeEnsureTrueTest() {
        Birthplace porto = new Birthplace("Porto");
        int expectedHash = 77301773;
        int actualHash = porto.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for toString - Ensure true")
    void toStringEnsureTrueTest() {
        //Arrange
        Birthplace porto = new Birthplace("Porto");
        String expected = "Birthplace{birthplace='Porto'}";
        //Act
        String actual = porto.toString();
        //Assert
        assertEquals(expected, actual);
    }
}