package project.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.Birthdate;
import project.model.entities.shared.Designation;

import java.time.format.DateTimeParseException;

import static org.junit.jupiter.api.Assertions.*;

class BirthdateTest {

    /**
     * Test for Birthdate Constructor
     * Happy case (valid BirthDate)
     */
    @Test
    @DisplayName("Test for Birthdate constructor - Happy case")
    void BirthdateConstructorHappyCaseTest() {
        Birthdate birthdate = new Birthdate("1993-03-15");
        assertTrue(birthdate instanceof Birthdate);
    }

    /**
     * Test for Birthdate Constructor
     * Exception with invalid BirthDate - null case
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with null BirthDate")
    void BirthdateConstructorEnsureExceptionWithNullTest() {
        assertThrows(NullPointerException.class, () -> {
            Birthdate birthdate = new Birthdate(null);
        });
    }

    /**
     * Test for Birthdate Constructor
     * Exception with invalid BirthDate - empty String
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with empty BirthDate")
    void BirthdateConstructorEnsureExceptionWithEmptyTest() {
        assertThrows(DateTimeParseException.class, () -> {
            Birthdate birthdate = new Birthdate("");
        });
    }

    /**
     * Test for Birthdate Constructor
     * Exception with invalid BirthDate - only whitespaces
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with only whitespaces BirthDate")
    void BirthdateConstructorEnsureExceptionWithOnlyWhitespacesTest() {
        assertThrows(DateTimeParseException.class, () -> {
            Birthdate birthdate = new Birthdate("     ");
        });
    }

    /**
     * Test for Birthdate Constructor
     * Exception with invalid BirthDate - illegal pattern date
     */
    @Test
    @DisplayName("Test for the constructor - Ensure IllegalArgumentException with illegal pattern BirthDate")
    void BirthdateConstructorEnsureExceptionWithIllegalPatternTest() {
        assertThrows(DateTimeParseException.class, () -> {
            Birthdate birthdate = new Birthdate("12-12-2000");
        });
    }

    /**
     * Test for Birthdate equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same Object")
    void BirthdateEqualsSameObjectTest() {
        //Arrange
        Birthdate birthdate = new Birthdate("1993-03-15");
        //Act
        boolean result = birthdate.equals(birthdate);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for Birthdate equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - Ensure true with objects with same attribute")
    void BirthdateEqualsSameObjectsTest() {
        //Arrange
        Birthdate birthdate = new Birthdate("1993-03-15");
        Birthdate filipe = new Birthdate("1993-03-15");
        //Act
        boolean result = filipe.equals(birthdate);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for Birthdate equals
     * Null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null object")
    void BirthdateNullTest() {
        //Arrange
        Birthdate birthdate = null;
        Birthdate filipe = new Birthdate("1993-03-15");
        //Act
        boolean result = filipe.equals(birthdate);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for Birthdate equals
     * Objects from different classes
     */
    @Test
    @DisplayName("Test for equals - Ensure false with objects from different classes")
    void BirthdateObjectsDifferentClassTest() {
        //Arrange
        Birthdate birthdate = new Birthdate("1993-03-15");
        Designation food = new Designation("Pizza");
        //Act
        boolean result = birthdate.equals(food);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for Birthdate hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Happy Case")
    void BirthdateHashCodeEnsureTrueTest() {
        Birthdate birthdate = new Birthdate("1993-03-15");
        int expectedHash = 4081902;
        int actualHash = birthdate.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for Birthdate hashcode
     * Ensure Not Equals
     */
    @Test
    @DisplayName("Test for hashcode - Ensure Not Equals")
    void BirthdateHashCodeEnsureNotEqualsTest() {
        Birthdate birthdate = new Birthdate("1993-03-15");
        int expectedHash = -1111;
        int actualHash = birthdate.hashCode();
        assertNotEquals(expectedHash, actualHash);
    }

    /**
     * Test for Birthdate toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for Birthdate toString  - Ensure true")
    void toStringBirthdateEnsureTrueTest() {
        //Arrange
        Birthdate birthdate = new Birthdate("1989-07-31");
        String expected = "Birthdate{birthdate='1989-07-31'}";
        //Act
        String actual = birthdate.toString();
        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for Birthdate toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for Birthdate toString  - Ensure false")
    void toStringBirthdateEnsureFalseTest() {
        //Arrange
        Birthdate birthdate = new Birthdate("1989-07-31");
        String expected = "Birthdate{birthdate='199999999999-07-31'}";
        //Act
        String actual = birthdate.toString();
        //Assert
        assertNotEquals(expected, actual);
    }

}