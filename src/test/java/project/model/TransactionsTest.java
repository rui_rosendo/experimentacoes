package project.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.exceptions.TransactionAlreadyExistsException;
import project.model.entities.Transactions;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TransactionsTest {

    private Transactions newTransactions;
    private AccountID account1ID;
    private AccountID account2ID;
    private Category groceries;
    private Category gym;

    @BeforeEach
    public void init() {
        newTransactions = new Transactions();
        account1ID = new AccountID("1");
        account2ID = new AccountID("2");
        groceries = new Category("Groceries");
        gym = new Category("Gym");

    }

    /**
     * Test for addTransaction Method
     * Happy case
     */
    @Test
    void addTransactionHappyCaseTest() {
        //ARRANGE
        Category newCategory = new Category("Groceries");
        Transaction t1 = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for January",
                newCategory, account1ID, account2ID);
        int newTransactionsInitialSize = newTransactions.size();

        //ACT
        newTransactions.addTransaction(t1);
        //ASSERT
        assertEquals(newTransactionsInitialSize + 1, newTransactions.size());

    }

    /**
     * Test for addTransaction Method
     * Fail case - Impossible to add a transaction that already exists
     */
    @Test
    void addTransactionTryingToAddTransactionThatAlreadyExistsTest() {
        //ARRANGE
        Category newCategory = new Category("Groceries");
        Transaction t1 = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for January",
                newCategory, account1ID, account2ID);
        newTransactions.addTransaction(t1);
        //ACT
        //ASSERT
        assertThrows(TransactionAlreadyExistsException.class, () -> {
            newTransactions.addTransaction(t1);
        });

    }

    /**
     * Test for addTransaction Method
     * Trying to add two transactions
     */
    @Test
    void addTransactionTryingToAddMoreThanOneTransactionTest() {
        //ARRANGE
        Transaction t1 = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for January", groceries,
                account1ID, account2ID);
        Transaction t2 = new Transaction("30", Type.DEBIT, "2020-02-03 00:00:00", "Pay gym for January", gym,
                account1ID, account2ID);
        int newTransactionsInitialSize = newTransactions.size();

        //ACT
        newTransactions.addTransaction(t1);
        newTransactions.addTransaction(t2);
        //ASSERT
        assertEquals(newTransactionsInitialSize + 2, newTransactions.size());
    }

    /**
     * Test for size
     * Happy case
     */
    @Test
    void sizeOfTransactionsSetHappyCaseTest() {
        //ARRANGE
        Transaction t1 = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for January", groceries,
                account1ID, account2ID);
        Transaction t2 = new Transaction("30", Type.DEBIT, "2020-02-03 00:00:00", "Pay gym - January", gym,
                account1ID, account2ID);
        Transaction t3 = new Transaction("30", Type.DEBIT, "2020-02-03 00:00:00", "Pay gym - February", gym,
                account1ID, account2ID);
        //ACT
        newTransactions.addTransaction(t1);
        newTransactions.addTransaction(t2);
        newTransactions.addTransaction(t3);
        int expected = 3;
        int result = newTransactions.size();
        //ASSERT
        assertEquals(expected, result);
    }

//    /**
//     * Test for size
//     * Trying to add an existing transaction- check if size maintains the same
//     */
//    @Test
//    void sizeOfTransactionsSetSadCaseTest() {
//        //ARRANGE
//        Transaction t1 = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for January",
//        groceries, account1ID, account2ID);
//        Transaction t2 = new Transaction("30", Type.DEBIT, "2020-02-03 00:00:00", "Pay gym - January", gym,
//        account1ID, account2ID);
//        //ACT
//        newTransactions.addTransaction(t1);
//        newTransactions.addTransaction(t2);
//        newTransactions.addTransaction(t2);
//        int expected = 2;
//        int result = newTransactions.size();
//        //ASSERT
//        assertEquals(expected, result);
//    }

    /**
     * Test for getAll Transactions Method
     * Happy case
     */
    @Test
    void getAllTransactionsHappyCaseTest() {
        //ARRANGE
        Transaction t1 = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for January", groceries,
                account1ID, account2ID);
        Transaction t2 = new Transaction("30", Type.DEBIT, "2020-02-03 00:00:00", "Pay gym - January", gym,
                account1ID, account2ID);
        Transaction t3 = new Transaction("30", Type.DEBIT, "2020-02-03 00:00:00", "Pay gym - February", gym,
                account1ID, account2ID);
        newTransactions.addTransaction(t1);
        newTransactions.addTransaction(t2);
        newTransactions.addTransaction(t3);
        //ACT
        List<Transaction> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        List<Transaction> result = newTransactions.getAll();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getAll Transactions Method
     * Sad case
     */
    @Test
    void getAllTransactionsSadCaseTest() {
        //ARRANGE
        Transaction t1 = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for January", groceries,
                account1ID, account2ID);
        Transaction t2 = new Transaction("30", Type.DEBIT, "2020-02-03 00:00:00", "Pay gym - January", gym,
                account1ID, account2ID);
        Transaction t3 = new Transaction("30", Type.DEBIT, "2020-02-03 00:00:00", "Pay gym - February", gym,
                account1ID, account2ID);
        newTransactions.addTransaction(t1);
        newTransactions.addTransaction(t3);
        //ACT
        List<Transaction> expected = new ArrayList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        List<Transaction> result = newTransactions.getAll();
        //ASSERT
        assertNotEquals(expected, result);
    }

    @Test
    void toStringHappyCase() {
        //Arrange
        Transaction t1 = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for January", groceries,
                account1ID, account2ID);
        newTransactions.addTransaction(t1);
        String expected = "Transactions{transactions=[Transaction{amount=Amount{amount=20.0}, " +
                "dateTime=DateTime{dateTime='2021-01-08T00:00'}, type=Type{type=-1}, " +
                "description=Description{description='Groceries for January'}, " +
                "category=Category{designation=Designation{designation='Groceries'}}," +
                " debitAccountID=AccountID{accountID=1}, creditAccountID=AccountID{accountID=2}}]}";
        //Act
        String result = newTransactions.toString();
        //Assert
        assertEquals(expected, result);
    }
}