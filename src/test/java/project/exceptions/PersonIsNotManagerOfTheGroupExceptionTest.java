package project.exceptions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PersonIsNotManagerOfTheGroupExceptionTest {

    /**
     * Test for constructor filled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor filled - Happy case")
    void constructorFilledTest() {
        //ARRANGE
        PersonIsNotManagerOfTheGroupException ex = new PersonIsNotManagerOfTheGroupException("This is an error message");

        // ACT
        //ASSERT
        assertTrue(ex instanceof PersonIsNotManagerOfTheGroupException);
    }

    /**
     * Test for constructor unfilled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor unfilled - Happy case")
    void constructorUnfilledTest() {
        //ARRANGE
        PersonIsNotManagerOfTheGroupException ex = new PersonIsNotManagerOfTheGroupException();

        // ACT
        //ASSERT
        assertTrue(ex instanceof PersonIsNotManagerOfTheGroupException);
    }

    /**
     * Test for getMessage with filled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with filled constructor - Happy case")
    void getMessageFilledConstructorHappyCase() {
        //ARRANGE
        PersonIsNotManagerOfTheGroupException ex = new PersonIsNotManagerOfTheGroupException("This is an error message");
        String expected = "This is an error message";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getMessage with unfilled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with unfilled constructor - Happy case")
    void getMessageUnfilledConstructorHappyCase() {
        //ARRANGE
        PersonIsNotManagerOfTheGroupException ex = new PersonIsNotManagerOfTheGroupException();
        String expected = "Person is not manager of the group";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

}