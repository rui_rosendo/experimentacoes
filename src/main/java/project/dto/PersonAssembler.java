package project.dto;

import org.springframework.hateoas.Link;
import project.model.entities.person.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonAssembler {

    /**
     * Constructor for PersonAssembler
     * * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    PersonAssembler() {
        //Intentionally empty
    }

    /**
     * mapToDTO method
     *
     * @param email
     * @param name
     * @param address
     * @param birthDate
     * @param birthplace
     * @return
     */
    public static GetPersonInfoResponseDTO mapToResponseDTO(String email, String name, String address, String birthDate, String birthplace) {
        return new GetPersonInfoResponseDTO(email, name, address, birthDate, birthplace);
    }


    public static PersonsResponseDTO mapToDTO(List<Person> persons) {
        List<GetPersonInfoResponseDTO> personsDTOs = new ArrayList<>();

        GetPersonInfoResponseDTO personDTO;

        for (Person person : persons) {
            personDTO = new GetPersonInfoResponseDTO(person.getID().toStringDTO(),
                    person.getName().toStringDTO(),
                    person.getAddress().getAddressValue(),
                    person.getBirthDate().getBirthdateValue(),
                    person.getBirthPlace().getBirthplaceValue()
            );

            personsDTOs.add(personDTO);
        };


        return new PersonsResponseDTO(personsDTOs);
    }
}
