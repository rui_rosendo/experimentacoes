package project.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@EqualsAndHashCode(callSuper = false)
public class GroupDTOMinimal extends RepresentationModel<GroupDTOMinimal> {

    private final String groupID;
    private final String description;
    private final String creationDate;

    /**
     * Constructor of GetPersonsGroupsDTO class
     *
     * @param groupID      - Identity of the new group as a String
     * @param description  - Description of the new group to be created as a String
     * @param creationDate - Creation date of the new group to be created as a String
     */
    public GroupDTOMinimal(String groupID, String description, String creationDate) {
        this.groupID = groupID;
        this.description = description;
        this.creationDate = creationDate;
    }

    /**
     * Method toString
     */
    @Override
    public String toString() {
        return "GroupDTOMinimal{" +
                "groupID='" + groupID + '\'' +
                ", description='" + description + '\'' +
                ", creationDate='" + creationDate + '\'' +
                '}';
    }
}
