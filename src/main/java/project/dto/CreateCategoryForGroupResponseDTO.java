package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CreateCategoryForGroupResponseDTO extends RepresentationModel<CreateCategoryForGroupResponseDTO> {

    private String groupID;
    private String groupDescription;
    private String newCategory;

    /**
     * Constructor for CreateCategoryResponseDTO
     *
     * @param groupID
     * @param groupDescription
     * @param newCategory
     */
    public CreateCategoryForGroupResponseDTO(String groupID, String groupDescription, String newCategory) {
        setGroupID(groupID);
        setGroupDescription(groupDescription);
        setNewCategory(newCategory);
    }

    /**
     * Method getGroupID
     *
     * @return object groupID
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * Method setGroupID
     *
     * @param groupID String to be converted to long
     */
    private void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * Method getGroupDescription
     *
     * @return groupDescription of the Group
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * Method setGroupDescription
     *
     * @param groupDescription groupDescription of the Category
     */
    private void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    /**
     * Method getNewCategory
     *
     * @return object newCategory
     */
    public String getNewCategory() {
        return newCategory;
    }

    /**
     * Method setNewCategory
     *
     * @param newCategory newCategory of the Category
     */
    private void setNewCategory(String newCategory) {
        this.newCategory = newCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateCategoryForGroupResponseDTO that = (CreateCategoryForGroupResponseDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(newCategory, that.newCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupID, groupDescription, newCategory);
    }
}
