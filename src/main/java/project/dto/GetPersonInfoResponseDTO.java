package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class GetPersonInfoResponseDTO extends RepresentationModel<GetPersonInfoResponseDTO> {
    private String email;
    private String name;
    private String address;
    private String birthplace;
    private String birthDate;

    /**
     * Constructor of GetPersonInfoResponseDTO class
     *
     * @param email
     * @param name
     * @param address
     * @param birthDate
     * @param birthplace
     */
    public GetPersonInfoResponseDTO(String email, String name, String address, String birthDate, String birthplace) {
        setEmail(email);
        setName(name);
        setAddress(address);
        setBirthDate(birthDate);
        setBirthplace(birthplace);
    }

    /**
     * getEmail method
     *
     * @return email as String
     */
    public String getEmail() {
        return email;
    }

    /**
     * setEmail method
     * sets the value of GetPersonInfoResponseDTO's name attribute to contain the name as a String
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * getName method
     *
     * @return name as String
     */
    public String getName() {
        return name;
    }

    /**
     * setName method
     * sets the value of GetPersonInfoResponseDTO's name attribute to contain the name as a String
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getName getAddress
     *
     * @return address as String
     */
    public String getAddress() {
        return address;
    }

    /**
     * setAddress method
     * sets the value of GetPersonInfoResponseDTO's address attribute to contain the address as a String
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * getName getBirthplace
     *
     * @return birthplace as String
     */
    public String getBirthplace() {
        return birthplace;
    }

    /**
     * setBirthplace method
     * sets the value of GetPersonInfoResponseDTO's birthplace attribute to contain the birthplace as a String
     *
     * @param birthplace
     */
    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    /**
     * getName getBirthDate
     *
     * @return birthDate as String
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * setBirthDate method
     * sets the value of GetPersonInfoResponseDTO's birthDate attribute to contain the birthDate as a String
     *
     * @param birthDate
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Override of equals method
     *
     * @param o an Object
     * @return True if this Object is equal Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetPersonInfoResponseDTO personDTO = (GetPersonInfoResponseDTO) o;
        return Objects.equals(email, personDTO.email) &&
                Objects.equals(name, personDTO.name) &&
                Objects.equals(address, personDTO.address) &&
                Objects.equals(birthplace, personDTO.birthplace) &&
                Objects.equals(birthDate, personDTO.birthDate);
    }

    /**
     * Override of hashCode method
     *
     * @return an integer with the hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), email, name, address, birthplace, birthDate);
    }

}
