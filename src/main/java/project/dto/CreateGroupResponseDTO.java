package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CreateGroupResponseDTO extends RepresentationModel<CreateGroupResponseDTO> {

    private String groupID;
    private String description;

    /**
     * Constructor for CreateGroupResponseDTO
     *
     * @param groupID     Id of the created group
     * @param description Description of the created group
     */
    public CreateGroupResponseDTO(String groupID, String description) {
        setGroupID(groupID);
        setDescription(description);
    }

    /**
     * getGroupID method
     *
     * @return the group's ID as a String
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * setGroupID method
     * sets the value of GroupDTO's groupID attribute to contain the groupID as a String
     *
     * @param groupID
     */
    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * getDescription method
     *
     * @return the group's description as a String
     */
    public String getDescription() {
        return description;
    }

    /**
     * setDescription method
     * sets the value of CreateGroupResponseDTO description attribute to contain the description of the group as a String
     *
     * @param description the group's description as a String
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateGroupResponseDTO that = (CreateGroupResponseDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupID, description);
    }
}
