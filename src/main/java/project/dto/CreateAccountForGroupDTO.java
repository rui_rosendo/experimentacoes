package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class CreateAccountForGroupDTO extends RepresentationModel<CreateAccountForGroupDTO> {

    private Set<String> accountsIDs;


    /**
     * Constructor for CreateAccountForGroupDTO
     *
     */
    public CreateAccountForGroupDTO(Set<String> accountsIDs) {
        setAccountsIDs(accountsIDs);

    }

    /**
     * Method getAccountsIDs
     *
     * @return copy of accountsIDs
     */
    public Set<String> getAccountsIDs() {
        return Collections.unmodifiableSet(accountsIDs);
    }

    /**
     * Method setAccountsIDs
     *
     * @param accountsIDs attribute
     */
    public void setAccountsIDs(Set<String> accountsIDs) {
        this.accountsIDs = Collections.unmodifiableSet(accountsIDs);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateAccountForGroupDTO that = (CreateAccountForGroupDTO) o;
        return Objects.equals(accountsIDs, that.accountsIDs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountsIDs);
    }

}
