package project.dto;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.List;

@Getter
@EqualsAndHashCode(callSuper = false)
public class PersonsResponseDTO extends RepresentationModel<PersonsResponseDTO> {

    private List<GetPersonInfoResponseDTO> persons;

    public PersonsResponseDTO (List<GetPersonInfoResponseDTO> persons) {
        this.persons = Collections.unmodifiableList(persons);
    }

    @Override
    public String toString() {
        return "PersonsResponseDTO{" +
                "persons=" + persons +
                '}';
    }

}
