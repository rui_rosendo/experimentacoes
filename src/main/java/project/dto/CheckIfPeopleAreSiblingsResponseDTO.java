package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CheckIfPeopleAreSiblingsResponseDTO extends RepresentationModel<CheckIfPeopleAreSiblingsResponseDTO> {
    String isSibling;

    /**
     * Constructor CheckIfPeopleAreSiblingsResponseDTO
     *
     * @param isSibling (true/false)
     */
    public CheckIfPeopleAreSiblingsResponseDTO(String isSibling) {
        setIsSibling(isSibling);
    }

    /**
     * Method getIsSibling
     *
     * @return CheckIfPeopleAreSiblingsResponseDTO
     */
    public String getIsSibling() {
        return isSibling;
    }

    /**
     * Method setIsSibling
     *
     * @param isSibling (true/false)
     */
    private void setIsSibling(String isSibling) {
        this.isSibling = isSibling;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckIfPeopleAreSiblingsResponseDTO that = (CheckIfPeopleAreSiblingsResponseDTO) o;
        return Objects.equals(isSibling, that.isSibling);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isSibling);
    }
}
