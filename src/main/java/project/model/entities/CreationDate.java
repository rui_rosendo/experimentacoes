package project.model.entities;

import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@NoArgsConstructor
@Embeddable
public class CreationDate {

    private LocalDate creationDateValue;

    /**
     * Constructor for CreationDate
     * throw a exception if the string (null, empty, composed by just spaces or not format valid)
     * Calls isDateValid for validation
     *
     * @param creationDate Creation date of a Group
     */
    public CreationDate(String creationDate) {
        setCreationDateValue(creationDate);
    }

    /**
     * Create attribute creationDate
     *
     * @param creationDateValue string in format of creation date
     */
    private void setCreationDateValue(String creationDateValue) {
        this.creationDateValue = LocalDate.parse(creationDateValue, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreationDate that = (CreationDate) o;
        return Objects.equals(creationDateValue, that.creationDateValue);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(creationDateValue);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "CreationDate{" +
                "creationDate='" + creationDateValue + '\'' +
                '}';
    }

    public String toStringDTO() {
        return creationDateValue.toString();
    }
}