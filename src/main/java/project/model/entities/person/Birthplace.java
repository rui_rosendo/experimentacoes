package project.model.entities.person;

import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import java.util.Objects;

public class Birthplace implements ValueObject {

    String birthplaceValue;

    /**
     * Constructor for Class Birthplace
     *
     * @param birthplace Place of birth
     */
    public Birthplace(String birthplace) {
        setBirthplaceValue(birthplace);
    }

    /**
     * Create attribute birthplace
     * throw a exception if the string (null, empty or composed by just spaces)
     *
     * @param birthplaceValue birthplace of person
     */
    private void setBirthplaceValue(String birthplaceValue) {
        if (birthplaceValue != null && !birthplaceValue.isEmpty() && !birthplaceValue.trim().isEmpty()) {
            this.birthplaceValue = birthplaceValue;
        } else {
            throw new InvalidFieldException("Input 'birthplace' is invalid!");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Birthplace that = (Birthplace) o;
        return Objects.equals(birthplaceValue, that.birthplaceValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(birthplaceValue);
    }

    @Override
    public String toString() {
        return "Birthplace{" +
                "birthplace='" + birthplaceValue + '\'' +
                '}';
    }

    public String getBirthplaceValue() {
        return birthplaceValue;
    }
}
