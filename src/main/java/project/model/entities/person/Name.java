package project.model.entities.person;

import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import java.util.Objects;

public class Name implements ValueObject {
    String nameValue;

    public Name(String name) {
        setNameValue(name);
    }

    /**
     * Create attribute name
     * Calls isStringValid for validation
     * throw a exception if the string(null, empty or composed by just spaces)
     *
     * @param nameValue Name of the Person
     */
    private void setNameValue(String nameValue) {
        if (nameValue != null && !nameValue.isEmpty() && !nameValue.trim().isEmpty()) {
            this.nameValue = nameValue;
        } else {
            throw new InvalidFieldException("Input 'name' is invalid!");
        }
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Name that = (Name) o;
        return Objects.equals(nameValue, that.nameValue);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(nameValue);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Name{" +
                "name='" + nameValue + '\'' +
                '}';
    }

    /**
     * toStringDTO
     */
    public String toStringDTO() {
        return nameValue;
    }
}
