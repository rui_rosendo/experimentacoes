package project.model.entities.shared;

import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class AccountsIDs implements ValueObject {

    private final Set<AccountID> accountsIDsValue;

    /**
     * Constructor for AccountsIDs
     */
    public AccountsIDs() {
        this.accountsIDsValue = new HashSet<>();
    }

    /**
     * Method getAccountsIDs
     *
     * @return
     */
    public Set<AccountID> getAccountsIDsValue() {
        return Collections.unmodifiableSet(accountsIDsValue);
    }

    /**
     * method for add Account ID
     *
     * @param accountID account ID
     * @return true/false
     */
    public boolean addAccountID(AccountID accountID) {
        if (accountID != null) {
            return this.accountsIDsValue.add(accountID);
        } else {
            throw new InvalidFieldException("AccountID can't be 'null'");
        }
    }


    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountsIDs that = (AccountsIDs) o;
        return Objects.equals(accountsIDsValue, that.accountsIDsValue);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(accountsIDsValue);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "AccountsIDs{" +
                "AccountsIDs=" + accountsIDsValue +
                '}';
    }
}
