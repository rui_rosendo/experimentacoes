package project.model.entities.ledger;

import lombok.Data;
import lombok.NoArgsConstructor;
import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@NoArgsConstructor
@Embeddable
public class Amount implements ValueObject, Serializable {

    double amountValue;

    /**
     * Constructor for Amount
     *
     * @param amount - double
     */
    public Amount(String amount) {
        Double parsedAmount = Double.parseDouble(amount);
        if (parsedAmount > 0) {
            this.amountValue = parsedAmount;
        } else {
            throw new InvalidFieldException("Input 'amount' is invalid!");
        }
    }

    /**
     * Override of toString
     */
    @Override
    public String toString() {
        return "Amount{" +
                "amount=" + amountValue +
                '}';
    }

    /**
     * Method toStringDTO
     */
    public String toStringDTO() {
        return "" + amountValue;
    }

    /**
     * Override of equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Amount that = (Amount) o;
        return Double.compare(that.amountValue, amountValue) == 0;
    }

    /**
     * Override of hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(amountValue);
    }
}
