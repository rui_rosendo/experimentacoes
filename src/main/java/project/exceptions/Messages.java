package project.exceptions;

public final class Messages {

    public static final String GROUPCONFLICT = "Group conflict";
    public static final String ACCOUNTALREADYEXISTS = "Account already exists";
    public static final String ACCOUNTNOTFOUND = "Account not found";
    public static final String CATEGORYALREADYEXISTS = "Category already exists";
    public static final String CATEGORYNOTFOUND = "Category not found";
    public static final String GROUPALREADYEXISTS = "Group already exists";
    public static final String GROUPNOTFOUND = "Group not found";
    public static final String MEMBERALREADYEXISTS = "Member already exists";
    public static final String PERSONNOTFOUND = "Person not found";
    public static final String PERSONNOTMANAGER = "Person is not manager of the group";
    public static final String INVALIDFIELD = "Invalid field";
    public static final String GROUPLEDGERALREADYEXISTS = "Group ledger already exists";
    public static final String LEDGERNOTFOUND = "Ledger not found";
    public static final String TRANSACTIONALREADYEXISTS = "Transaction already exists";
    public static final String ACCOUNTCONFLICT = "Account does not belong to entity";

    Messages() {
        // Throw new exception if private constructor is ever called (not supposed to)
        throw new IllegalStateException("Utility class");
    }
}
