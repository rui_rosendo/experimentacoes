package project.exceptions;

import static project.exceptions.Messages.CATEGORYNOTFOUND;

public class CategoryNotFoundException extends RuntimeException {
    static final long serialVersionUID = 5957353327579112498L;

    /**
     * Exception for when category already exists
     * Uses exceptions.Messages for exception message
     */
    public CategoryNotFoundException() {
        super(CATEGORYNOTFOUND);
    }

    /**
     * Exception for when category already exists
     * Uses provided message for exception message
     */
    public CategoryNotFoundException(String message) {
        super(message);
    }
}
