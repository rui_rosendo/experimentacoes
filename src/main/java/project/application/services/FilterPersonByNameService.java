package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.PersonAssembler;
import project.dto.PersonsResponseDTO;
import project.frameworkddd.IUSFilterPersonByNameService;
import project.model.entities.person.Person;
import project.model.specifications.repositories.PersonRepository;

import java.util.List;

@Service
public class FilterPersonByNameService implements IUSFilterPersonByNameService {

    @Autowired
    private PersonRepository personRepository;


    /**
     * Constructor for
     */
    public FilterPersonByNameService() {
        //Intentionally empty
    }

    public PersonsResponseDTO filterPersonByName(String filter) {

        List<Person> persons = personRepository.filterByName(filter);

        PersonsResponseDTO responseDTO = PersonAssembler.mapToDTO(persons);

        return responseDTO;
    }

    public PersonsResponseDTO getPeople() {

        List<Person> persons = personRepository.findAll();

        PersonsResponseDTO responseDTO = PersonAssembler.mapToDTO(persons);

        return responseDTO;
    }

}
