package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CreateCategoryForGroupAssembler;
import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonIsNotManagerOfTheGroupException;
import project.frameworkddd.IUSCreateCategoryForPersonService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;

import java.util.Optional;

@Service
public class CreateCategoryForGroupService implements IUSCreateCategoryForPersonService {

    @Autowired
    GroupRepository groupRepository;

    /**
     * Constructor for CreateCategoryForGroupService
     *
     * @param groupRepository Instance of GroupRepository containing all groups
     */
    public CreateCategoryForGroupService(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    /**
     * Method createCategoryForGroup
     * Add a Category to categories of the group
     * verify if groupRepository contains groupID
     *
     * @param requestDTO CreateCategoryForGroupRequestDTO
     * @return If the group exists in the groupRepository, the person is manager of the group, but the category already exists: throws CategoryAlreadyExistsException
     */
    public CreateCategoryForGroupResponseDTO createCategoryForGroup(CreateCategoryForGroupRequestDTO requestDTO) {
        GroupID groupID = new GroupID(requestDTO.getGroupID());

        Optional<Group> opGroup = groupRepository.findById(groupID);

        if (opGroup.isPresent()) {
            Group group = opGroup.get();
            PersonID personID = new PersonID(requestDTO.getPersonEmail());
            if (group.hasManagerID(personID)) {
                String designation = requestDTO.getDesignation();
                if (group.addCategory(designation)) {
                    Group savedGroup = groupRepository.save(group);
                    return CreateCategoryForGroupAssembler.mapToResponseDTO(
                            savedGroup.getID().toStringDTO(),
                            savedGroup.getDescription().getDescriptionValue(),
                            designation);
                } else {
                    throw new CategoryAlreadyExistsException();
                }
            } else {
                throw new PersonIsNotManagerOfTheGroupException();
            }
        }
        throw new GroupNotFoundException();
    }


}
