package project.frameworkddd;


import project.dto.PersonsResponseDTO;

public interface IUSFilterPersonByNameService {

    PersonsResponseDTO filterPersonByName(String filter);

    PersonsResponseDTO getPeople();
}
