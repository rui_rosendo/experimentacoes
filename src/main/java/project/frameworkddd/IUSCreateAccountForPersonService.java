package project.frameworkddd;

import project.dto.AccountsDTO;
import project.dto.CreateAccountForPersonRequestDTO;
import project.dto.CreateAccountForPersonResponseDTO;

public interface IUSCreateAccountForPersonService {
    CreateAccountForPersonResponseDTO createAccountForPerson(CreateAccountForPersonRequestDTO requestDTO);

    AccountsDTO getAccountsByPersonID(String personID);
}
