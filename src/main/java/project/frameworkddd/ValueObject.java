package project.frameworkddd;

/**
 * Interface for a value object which is a pattern in Domain-Driven Design. A value object is defined by its attributes
 * and has conceptually no identity and no life cycle (they should be immutable). When needed to be changed the VO
 * should be discarded and a new one created with new attributes.
 */
public interface ValueObject {

    @Override
    String toString();

    @Override
    boolean equals(Object other);

    @Override
    int hashCode();
}
