package project.frameworkddd;

import project.dto.GetFamilyGroupsDTO;

public interface IUSGetFamilyGroupsService {
    GetFamilyGroupsDTO getFamilyGroups();

}
