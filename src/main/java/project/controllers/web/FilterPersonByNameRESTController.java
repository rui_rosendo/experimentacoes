package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.PersonsResponseDTO;
import project.frameworkddd.IUSFilterPersonByNameService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class FilterPersonByNameRESTController {

    @Autowired
    private IUSFilterPersonByNameService service;

    @GetMapping("/people/filterName")
    public ResponseEntity<Object> getPeopleByNameFilter( @RequestParam(name = "filter") String filter) {
        PersonsResponseDTO responseDTO = service.filterPersonByName(filter);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @GetMapping("/people")
    public ResponseEntity<Object> getPeople() {
        PersonsResponseDTO responseDTO = service.getPeople();
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
