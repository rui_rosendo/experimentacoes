@startuml
skinparam defaultFontSize 16

skinparam sequence {
   ArrowColor blue
   LifeLineBorderColor blue
   ParticipantBorderColor blue
   ParticipantBackgroundColor transparent
   ParticipantFontSize 16
   ActorFontColor blue
   ActorFontSize 15
   ActorFontName blue }
hide footbox

Title US001 - As a system manager, I want to know if someone is another person's brother/sister. It is so if they have the same mother, or the same mother or father, or one is included in the list of siblings of the other. \nThe sibling relationship is bidirectional, that is, if A is B's sibling, therefore B is A's sibling.

autonumber

participant "HTTPClient" as test
participant "SpringBoot" as springBoot
participant ":CheckIfPeopleAreSiblingsRESTController" as controller
participant ":CheckIfPeopleAreSiblingsService" as service
participant "CheckIfPeopleAreSiblingsAssembler" as assembler
participant "requestDTO\n:CheckIfPeopleAreSiblingsRequestDTO" as requestDTO
participant "personID1\n:PersonID" as pid1
participant "personID2\n:PersonID" as pid2
participant "person1\n:Person" as p1
participant "responseDTO\n:CheckIfPeopleAreSiblingsResponseDTO" as responseDTO
participant "responseEntity\n:ResponseEntity" as responseEntity

activate test

test -> springBoot : HTTP Request \n(method:GET, URI: \n"/people/{personID1}/siblings/{personID2}")
activate springBoot

springBoot -> controller : checkIfPeopleAreSiblings\n(personID1, personID2)
activate controller
controller -> assembler : requestDTO = mapToRequestDTO \n (personID1, personID2)
activate assembler
assembler -> requestDTO *: (personID1, personID2)
deactivate assembler
controller -> service : checkPeopleAreSiblings (requestDTO)
activate service

service -> pid1* : (requestDTO.getPersonID1 ( ))
service -> pid2* : (requestDTO.getPersonID2 ( ))

service -> service : person1 = getPersonById (personID1)
ref over service : getPersonById
service -> service : person2 = getPersonById (personID2)
ref over service : getPersonById

service -> p1 : isSibling(person2)
activate p1
p1 --> service : result
deactivate p1

service -> assembler : responseDTO = mapToResponseDTO (result)
activate assembler
assembler -> responseDTO* : (result)
deactivate assembler

service --> controller: responseDTO
deactivate service

controller -> responseEntity* :  (responseDTO, HTTP.status.OK)
controller --> springBoot : responseEntity
deactivate controller
springBoot --> springBoot : serialize (responseEntity)
springBoot --> test : HTTP Response \n(ResponseBody, ResponseStatus)
deactivate springBoot

@enduml