package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.CreateCategoryForGroupAssembler;
import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.dto.CreateCategoryInfoDTO;
import project.frameworkddd.IUSCreateCategoryForPersonService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CreateCategoryForGroupRESTController {

    @Autowired
    private IUSCreateCategoryForPersonService service;

    /**
     * Method createCategoryForGroup
     *
     * @param info        an instance of CreateCategoryForGroupInfoDTO which represents a DTO from the client
     * @param personEmail personEmail in the Path
     * @param groupID     groupID in the Path
     * @return If the group exists in the groupRepository, the person is manager of the group, but the category already exists: throws CategoryAlreadyExistsException - HttpStatus 422.
     */
    @PostMapping("/people/{personEmail}/groups/{groupID}/categories")
    public ResponseEntity<Object> createCategoryForGroup(@RequestBody CreateCategoryInfoDTO info, @PathVariable String personEmail, @PathVariable String groupID) {
        CreateCategoryForGroupRequestDTO requestDTO = CreateCategoryForGroupAssembler.mapToRequestDTO(personEmail, groupID, info.getDesignation());
        CreateCategoryForGroupResponseDTO result = service.createCategoryForGroup(requestDTO);
        Link selfLink = linkTo(methodOn(GetCategoriesRESTController.class).getCategoriesByGroupID(groupID)).withSelfRel();
        result.add(selfLink);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}
