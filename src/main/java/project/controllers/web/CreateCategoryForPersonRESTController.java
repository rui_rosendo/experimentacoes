package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.CreateCategoryForPersonAssembler;
import project.dto.CreateCategoryForPersonRequestDTO;
import project.dto.CreateCategoryForPersonResponseDTO;
import project.dto.CreateCategoryInfoDTO;
import project.frameworkddd.IUSCreateCategoryForGroupService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CreateCategoryForPersonRESTController {

    @Autowired
    private IUSCreateCategoryForGroupService service;

    /**
     * Method createCategoryForGroup
     *
     * @param info        an instance of CreateCategoryForGroupInfoDTO which represents a DTO from the client
     * @param personEmail personEmail in the Path
     * @return If the group exists in the groupRepository, the person is manager of the group, but the category already exists: throws CategoryAlreadyExistsException - HttpStatus 422.
     */
    @PostMapping("/people/{personEmail}/categories")
    public ResponseEntity<Object> createCategoryForPerson(@RequestBody CreateCategoryInfoDTO info, @PathVariable String personEmail) {
        CreateCategoryForPersonRequestDTO requestDTO = CreateCategoryForPersonAssembler.mapToRequestDTO(personEmail, info.getDesignation());
        CreateCategoryForPersonResponseDTO result = service.createCategoryForPerson(requestDTO);
        Link selfLink = linkTo(methodOn(GetCategoriesRESTController.class).getCategoriesByPersonID(personEmail)).withSelfRel();
        result.add(selfLink);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}
