import React, {useContext, useEffect, useState} from "react";
import Button from "@material-ui/core/Button";
import {
    changeTransactionsFiltersFinalDate,
    changeTransactionsFiltersInitialDate,
    clearTransactionsFilters,
    filterTransactions
} from "../context/GroupActions";
import moment from "moment";
import FreeSoloGroup from "./FreeSoloGroup";
import DatePickerInlineGroup from "./DatePickerInlineGroup";
import GroupContext from "../context/GroupContext";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import {changeFilterName, clearPersonFilters, filterPeople, PERSON_NAME_FILTER} from "../context/AppActions";
import AppContext from "../context/AppContext";
import Input from "@material-ui/core/Input";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        direction: "row",
        justify: "space-evenly",
        alignItems: "center"
    },
    control: {
        align: "center"
    },
}));


function NamePersonFilter() {
    const {dispatch} = useContext(AppContext);
    const classes = useStyles();

    function handleSubmit(event) {
        event.preventDefault();
        dispatch(filterPeople())
    }

    function handleClear(event) {
        event.preventDefault();
        dispatch(clearPersonFilters())
    }

const handleChange = (event, filter) => {
        dispatch(changeFilterName(filter));
};

    return (
        <div>
            <Grid container className={classes.root} spacing={2}>
                <Grid item>
                    <Input className={classes.control} type="text" name="filter" id={'filter'}
                          onChange={handleChange}  placeholder="filter by name" required/>
                </Grid>
                <Grid item>
                    <Button id="applyFiltersBtn" variant="contained" color="primary" onClick={handleSubmit}>
                        Apply filters
                    </Button>
                </Grid>
                <Grid item>
                    <Button id="clearFiltersBtn" variant="contained" color="primary" onClick={handleClear}>
                        Clear filters
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default NamePersonFilter