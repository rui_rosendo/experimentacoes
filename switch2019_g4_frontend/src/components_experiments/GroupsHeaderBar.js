import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Logout from "../Logout";
import Avatar from "./Avatar";
import AppContext from "../context/AppContext";
import {Link} from "react-router-dom";
import PersonContext from "../context/PersonContext";
import logo from "../logo.png";
import PeopleButton from "./PeopleBotton";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function ButtonAppBar() {
    const classes = useStyles();
    const {username} = React.useContext(AppContext).state;
    const {groupId} = React.useContext(PersonContext).state;
    const {description} = React.useContext(PersonContext).state;

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Link to={`/home`} style={{color: 'white'}}><img src={logo} height="40" className="center"/></Link>
                    <Typography variant="h6" className={classes.title}>
                    </Typography>
                    <b>
                        <Link to={`/groups/${groupId}`} style={{color: 'white'}}><span>{description}</span></Link>
                        <span> / </span>
                        <Link to={`/home`} style={{color: 'white'}}><span>{username}</span></Link>
                    </b>
                    <pre>  </pre>
                    <Avatar/>
                    <PeopleButton/>
                    <Logout/>
                </Toolbar>
            </AppBar>
        </div>
    );
}
