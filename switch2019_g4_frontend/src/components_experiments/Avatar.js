import React, {Component} from 'react';

export default class Avatar extends Component {
    constructor(props) {
        super(props);
    }

    getRandomID() {
        return Math.floor(Math.random() * 150) + 1;
    }

    // Using local database
    render() {
        const avatarPath = require('../pokes/' + this.getRandomID() + '.gif');
        return (
            <div className="row">
                <div className="div">
                    <img src={avatarPath} className="img"/>
                </div>
            </div>
        )
    }

}



