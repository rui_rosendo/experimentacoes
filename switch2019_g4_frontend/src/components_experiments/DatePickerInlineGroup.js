import 'date-fns';
import React, {useContext, useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {KeyboardDatePicker, MuiPickersUtilsProvider,} from '@material-ui/pickers';
import {changeTransactionsFiltersFinalDate, changeTransactionsFiltersInitialDate} from "../context/GroupActions";
import moment from "moment";
import GroupContext from "../context/GroupContext";

export default function MaterialUIPickers(props) {
    const {state, dispatch} = useContext(GroupContext);
    const {transactionsFilters} = state;
    const {label} = props;

    useEffect(() => {
        dispatch(changeTransactionsFiltersInitialDate((moment(Date()).format('YYYY-MM-DD') + " 00:00:00")))
        dispatch(changeTransactionsFiltersFinalDate((moment(Date()).format('YYYY-MM-DD') + " 23:59:59")))
    }, [])

    const handleDateChange = (event, date) => {
        const convertedDate = moment(date).format('YYYY-MM-DD')
        if (props.name === "initialDate") {
            dispatch(changeTransactionsFiltersInitialDate((convertedDate) + " 00:00:00"));
        } else {
            dispatch(changeTransactionsFiltersFinalDate((convertedDate) + " 23:59:59"));
        }
    };

    function minDate() {
        let startDate
        if (props.name === "finalDate") {
            startDate = (transactionsFilters.initialDate)
            return startDate
        }
    }

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container justify="space-around">
                <KeyboardDatePicker
                    autoOk={true}
                    minDate={minDate()}
                    disableFuture={true}
                    disableToolbar
                    variant="inline"
                    format="yyyy/MM/dd"
                    margin="normal"
                    id="date-picker-inline"
                    label={label}
                    onChange={handleDateChange}
                    value={props.name === "initialDate" ? transactionsFilters.initialDate : transactionsFilters.finalDate}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />
            </Grid>
        </MuiPickersUtilsProvider>
    );
}