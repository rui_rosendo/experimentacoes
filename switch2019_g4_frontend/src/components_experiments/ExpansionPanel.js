import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EnhancedPersonGroupsTable from "./EnhancedPersonGroupsTable";
import EnhancedPersonCategoriesTable from "./EnhancedPersonCategoriesTable";
import EnhancedPersonAccountsTable from "./EnhancedPersonAccountsTable";
import EnhancedGroupCategoriesTable from "./EnhancedGroupCategoriesTable";
import EnhancedPersonTransactionsTable from "./EnhancedPersonTransactionsTable";
import EnhancedGroupTransactionsTable from "./EnhancedGroupTransactionsTable";
import EnhancedGroupAccountsTable from "./EnhancedGroupAccountsTable";
import EnhancedGroupMembersTable from "./EnhancedGroupMembersTable";
import PersonContext from "../context/PersonContext";
import Skeleton from "@material-ui/lab/Skeleton";
import EnhancedPeopleTable from "./EnhancedPeopleTable";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },

}));


export default function ControlledExpansionPanels(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const {aggregate} = props;

    const {state} = React.useContext(PersonContext);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    function SelectTable(aggregatePassed) {
        if (aggregate === "groups") {
            return (
                <EnhancedPersonGroupsTable aggregate={aggregatePassed}/>
            )
        } else if (aggregate === "accounts") {
            return (
                <EnhancedPersonAccountsTable aggregate={aggregatePassed}/>
            )
        } else if (aggregate === "accountsGroup") {
            return (
                <EnhancedGroupAccountsTable aggregate={aggregatePassed}/>
            )
        } else if (aggregate === "categoriesGroup") {
            return (
                <EnhancedGroupCategoriesTable aggregate={aggregatePassed}/>
            )
        } else if (aggregate === "categories") {
            return (
                <EnhancedPersonCategoriesTable aggregate={aggregatePassed}/>
            )
        } else if (aggregate === "transactions") {
            return (
                <EnhancedPersonTransactionsTable aggregate={aggregatePassed}/>
            )
        } else if (aggregate === "transactionsGroup") {
            return (
                <EnhancedGroupTransactionsTable aggregate={aggregatePassed}/>
            )
        } else if (aggregate === "groupMembers") {
            return (
                <EnhancedGroupMembersTable aggregate={aggregatePassed}/>
            )
        } else if (aggregate === "people")
            return (
                <EnhancedPeopleTable aggregate={aggregatePassed}/>
            )
    }

    if (state.info.loading) {
        return (
            <div>
                <Skeleton/>
                <Skeleton animation={true}/>
                <Skeleton animation="wave"/>
            </div>
        )
    } else {
        return (
            <div className={classes.root}>
                <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1bh-content"
                        id="panel1bh-header"
                    >
                        <Typography className={classes.heading} variant="h8">
                            <h4>{props.name}</h4>
                        </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        {SelectTable(props.aggregate)}
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}