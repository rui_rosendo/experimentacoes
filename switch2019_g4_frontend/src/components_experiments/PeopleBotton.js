import React from 'react';
import {useHistory} from 'react-router-dom';
import Button from "@material-ui/core/Button";

function PeopleButton() {
    let history = useHistory();
    const handleClick = () => {
        history.push("/people");
//       
    };

    return (<Button color="inherit" type="submit"  onClick={handleClick}>People</Button>);

}

export default PeopleButton;