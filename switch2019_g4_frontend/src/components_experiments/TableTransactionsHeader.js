import React, {useContext} from "react";
import PersonContext from "../context/PersonContext";

function TableTransactionsHeader() {
    const {transactionsHeaders} = useContext(PersonContext)
    const { amount, dateTime, type, description, category, debitAccount, creditAccount} = transactionsHeaders

    return(
        <thead>
        <tr>
            <th style={{width:"50px"}}>{amount}></th>
            <th style={{width:"200px"}}>{dateTime}></th>
            <th style={{width:"50px"}}>{type}></th>
            <th style={{width:"200px"}}>{description}></th>
            <th style={{width:"100px"}}>{category}></th>
            <th style={{width:"100px"}}>{debitAccount}></th>
            <th style={{width:"100px"}}>{creditAccount}></th>
        </tr>
        </thead>
    )
}

export default TableTransactionsHeader