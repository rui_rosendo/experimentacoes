import React, {useContext, useState} from "react";
//import AppContext from "../context/AppContext";
import PersonContext from "../context/PersonContext";
import {fetchGroupCategoriesAfterPost, URL_API} from "../context/GroupActions";
import GroupContext from "../context/GroupContext";
import AppContext from "../context/AppContext";
import SnackbarContext from "../context/SnackbarContext";
import Input from "@material-ui/core/Input";
import {
    CATEGORY_CREATION_FAILED,
    CATEGORY_SUCCESSFULLY_CREATED,
    showErrorSnackbar,
    showSuccessfulSnackbar
} from "../context/SnackbarActions";

function FromGroupCategories() {

    const {username} = useContext(AppContext).state;
    const {state} = useContext(PersonContext);
    const {groupId} = state;
    const {dispatch} = useContext(GroupContext);
    const snackDispatch = useContext(SnackbarContext).dispatch;

    const [inputData, setInputData] = useState({
        designation: ""
    });

    function handleChange(event) {
        const {name, value} = event.target;
        setInputData(prevInputData => {
            return {
                ...prevInputData,
                [name]: value
            }
        })
    }

    function handleSubmit(event) {
        event.preventDefault();
        fetch(`${URL_API}/people/${username}/groups/${groupId}/categories`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputData)
        }).then(res => res.json()).then(res => {
            res.message === undefined ?
                snackDispatch(showSuccessfulSnackbar(CATEGORY_SUCCESSFULLY_CREATED)) :
                snackDispatch(showErrorSnackbar(CATEGORY_CREATION_FAILED + res.message));
            console.log(res)
        })
            .catch(error => {
                console.log(error);
                snackDispatch(showErrorSnackbar(CATEGORY_CREATION_FAILED + error.message))
            }).then(() => {
            setInputData(prevInputData => {
                return {
                    designation: ""
                }
            });
            dispatch(fetchGroupCategoriesAfterPost())
        })

    }

    return (
        <form onSubmit={handleSubmit} id="groupCategoryForm">
            <div className="ModalHeader">
                <p>Create new Category</p>
            </div>
            <div>
                <label className="labelForm" htmlFor="designation">Designation:</label>
                <br/>
                <Input type="text" name="designation" value={inputData.designation}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <button className="buttonForm" id="submitGroupCategory">Submit</button>
        </form>
    )

}

export default FromGroupCategories;