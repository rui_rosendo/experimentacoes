import React, {useContext, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {URL_API} from "../context/PersonActions";
import PersonContext from "../context/PersonContext";
import {fetchGroupInfoFailure, fetchGroupInfoStarted, fetchGroupInfoSuccess} from "../context/GroupActions";
import GroupContext from "../context/GroupContext";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper1: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: "white",
        backgroundColor: "#3f51b5",
        fontWeight: "bold",
    },

    paper2: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: "black",
        backgroundColor: "white",
    },

}));

export default function CenteredGrid() {
    const classes = useStyles();

    const {groupId} = React.useContext(PersonContext).state;

    const {state, dispatch} = useContext(GroupContext);
    const {info} = state;
    const {data} = info;
    const rows = data;

    useEffect(() => {
        dispatch(fetchGroupInfoStarted());
        fetch(`${URL_API}/groups/${groupId}`)
            .then(res => res.json()).then(res => dispatch(fetchGroupInfoSuccess(res)))
            .catch(err => dispatch(fetchGroupInfoFailure(err.message)))
    }, []);

    return (
        <div className={classes.root}>
            <Grid container spacing={1}>
                <Grid item xs={6}>
                    <Paper className={classes.paper1}>DESCRIPTION</Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.paper1}>CREATION DATE</Paper>
                </Grid>
            </Grid>
            <Grid container spacing={1}>
                <Grid item xs={6}>
                    <Paper className={classes.paper2}>{rows.description}</Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.paper2}>{rows.creationDate}</Paper>
                </Grid>
            </Grid>
        </div>
    );
}
