import {
  LOGIN,
  LOGOUT,
  FETCH_PEOPLE_FAILURE,
  FETCH_PEOPLE_STARTED,
  FETCH_PEOPLE_SUCCESS,
  PERSON_NAME_FILTER, FILTER_PERSONSNAME, CLEAR_PERSON_FILTERS
} from './AppActions'
import {CLEAR_TRANSACTIONS_FILTERS, FILTER_TRANSACTIONS, TRANSACTIONS_ACCOUNTID_FILTER} from "./GroupActions";

function appReducer(state, action) {
  switch (action.type) {
    case LOGIN:
      //Retorna o isLogged como true, e mantém o restante estado
      return {
        ...state,
        isLogged: true,
        username: action.username,
      };
    case LOGOUT:
      return {
        ...state,
        isLogged: false,
        username: "",
      };
    case FETCH_PEOPLE_STARTED:
      return {
        ...state,
        persons: {
          loading: true,
          error: null,
          data: []
        }
      };
    case FETCH_PEOPLE_SUCCESS:
      return {
        ...state,
        persons: {
          loading: false,
          error: null,
          data: [...action.payload.data]
        }
      };
    case FETCH_PEOPLE_FAILURE:
      return {
        ...state,
        persons: {
          loading: false,
          error: action.payload.error,
          data: []
        }
      };
    case PERSON_NAME_FILTER:
      //Altera o accountID do transactionsFilters do estado
      return {
        ...state,
        persons: {
          ...state.persons,
          filter: action.filter
        }
      };
    case FILTER_PERSONSNAME:
      //Retorna o hasFilters como 'yes'
      return {
        ...state,
        persons: {
          ...state.persons,
          hasFilters: 'yes'
        }
      };
    case CLEAR_PERSON_FILTERS:
      return {
        ...state,
        persons: {
          hasFilters: 'no',
          filter: ''
        }
      };
    default:
      return state;
  }
}

export default appReducer;
