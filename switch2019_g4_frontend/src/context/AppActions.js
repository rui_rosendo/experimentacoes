export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';


export function login(username) {
  return {
    type: LOGIN,
    username: username
  }
}

export function logout() {
  return {
    type: LOGOUT,
  }
}

  export const FETCH_PEOPLE_STARTED = 'FETCH_PEOPLE_STARTED';
  export const FETCH_PEOPLE_SUCCESS = 'FETCH_PEOPLE_SUCCESS';
  export const FETCH_PEOPLE_FAILURE = 'FETCH_PEOPLE_FAILURE';


  export function fetchPeopleStarted() {
    return {
      type: FETCH_PEOPLE_STARTED,
    }
  }

  export function fetchPeopleSuccess(persons) {
    return {
      type: FETCH_PEOPLE_SUCCESS,
      payload: {
        data: [...persons]
      }
    }
  }

  export function fetchPeopleFailure(message) {
    return {
      type: FETCH_PEOPLE_FAILURE,
      payload: {
        error: message
      }
    }
  }

export const FILTER_PERSONSNAME = 'FILTER_PERSONSNAME';
export const PERSON_NAME_FILTER = 'PERSON_NAME_FILTER';
export const CLEAR_PERSON_FILTERS = 'CLEAR_PERSON_FILTERS';

export function changeFilterName(filter) {
  return {
    type: PERSON_NAME_FILTER,
    filter: filter
  }
}



export function filterPeople() {
  return {
    type: FILTER_PERSONSNAME,
  }
}

export function clearPersonFilters() {
  return {
    type: CLEAR_PERSON_FILTERS,
  }
}


