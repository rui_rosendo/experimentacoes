import React, {useReducer} from 'react';
import PropTypes from "prop-types";
import {Provider} from './AppContext';
import appReducer from './AppReducer';

const initialState = {
    persons: {
        loading:true,
        error: null,
        data: [],
        isSubmitted: 'no',
        hasFilters: 'no',
        filter: ""
    },
    isLogged: false,
    username: ""
};

const headCellsGroups = [
    {id: 'groupID', numeric: false, disablePadding: true, label: 'Group Id'},
    {id: 'description', numeric: false, disablePadding: true, label: 'Description'},
    {id: 'creationDate', numeric: false, disablePadding: true, label: 'Creation Date'},
];
const headCellsMembers = [
    {id: 'membersIDs', numeric: false, disablePadding: true, label: 'Email'},
    {id: 'names', numeric: false, disablePadding: true, label: 'Name'},
    {id: 'isManager', numeric: false, disablePadding: true, label: 'Manager'},
];

const headCellsCategories = [
    {id: 'designation', numeric: false, disablePadding: true, label: 'Designation'}
];

const headCellsTransactions = [
    {id: 'creditAccountID', numeric: true, disablePadding: true, label: 'Credit Account'},
    {id: 'debitAccountID', numeric: true, disablePadding: true, label: 'Debit Account'},
    {id: 'description', numeric: false, disablePadding: true, label: 'Description'},
    {id: 'category', numeric: true, disablePadding: true, label: 'Category'},
    {id: 'dateTime', numeric: true, disablePadding: true, label: 'Date'},
    {id: 'amount', numeric: true, disablePadding: true, label: 'Amount'},
];

const headCellsAccounts = [
    {id: 'accountID', numeric: false, disablePadding: true, label: 'Account Id'},
    {id: 'denomination', numeric: false, disablePadding: true, label: 'Denomination'},
    {id: 'description', numeric: false, disablePadding: true, label: 'Description'},
];

const categoriesHeaders = {
    designation: "Designation"
};

const groupsHeaders = {
    id: "ID",
    description: "Description",
    creationDate: "Creation Date",
};

const membersHeaders = {
    membersIDs: "Email",
    names: "Name",
    isManager: "Manager",

};

const transactionsHeaders = {
    creditAccount: "Credit Account ID",
    debitAccount: "Debit Account ID",
    description: "Description",
    category: "Category",
    dateTime: "Date",
    amount: "Amount",
};

const accountsHeaders = {
    id: "ID",
    denomination: "Denomination",
    description: "Description"

};

const headCellsPerson = [
    {id: 'email', numeric: false, disablePadding: true, label: 'Email'},
    {id: 'name', numeric: false, disablePadding: true, label: 'Name'},
    {id: 'address', numeric: false, disablePadding: true, label: 'Address'},
    {id: 'birthplace', numeric: false, disablePadding: true, label: 'Birthplace'},
    {id: 'birthDate', numeric: true, disablePadding: true, label: 'BirthDate'},
];

const personHeaders = {
    email : "Email",
    name: "Name",
    address: "Address",
    birthplace: "Birthplace",
    birthDate: "BirthDate",
};

const AppProvider = (props) => {
    const [state, dispatch] = useReducer(appReducer, initialState);
    return (
        <Provider value={{
            state,
            headCellsGroups,
            headCellsTransactions,
            headCellsCategories,
            headCellsAccounts,
            headCellsMembers,
            groupsHeaders,
            transactionsHeaders,
            categoriesHeaders,
            accountsHeaders,
            membersHeaders,
            headCellsPerson,
            personHeaders,
            dispatch
        }}>
            {props.children}
        </Provider>
    );
};
AppProvider.propTypes = {
    children: PropTypes.node,
};


export default AppProvider;
