import React from "react";

const GroupContext = React.createContext();
export const {Provider} = GroupContext;
export default GroupContext