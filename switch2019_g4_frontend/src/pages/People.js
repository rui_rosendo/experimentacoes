import React from 'react';
import HomePeople from "../components_experiments/HomePeople";
import ExpansionPanel from "../components_experiments/ExpansionPanel";

function People() {
    return(
        <div style={{marginLeft: "10%", marginRight: "10%"}}>
            <br/>
            <HomePeople/>
            <br/>
            <ExpansionPanel name={"PEOPLE"} aggregate='people'/>
            <br/>
        </div>
    )
}

export default People