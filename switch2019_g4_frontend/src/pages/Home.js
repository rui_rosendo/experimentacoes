import React from 'react';
import ExpansionPanel from "../components_experiments/ExpansionPanel";
import HeaderBar from "../components_experiments/HomeHeaderBar";
import PersonData from "../components_experiments/PersonData";

function Home() {

    return (
        <div style={{marginLeft: "10%", marginRight: "10%"}}>
            <br/>
            <HeaderBar/>
            <br/>
            <PersonData/>
            <br/>
            <ExpansionPanel name={"GROUPS"} aggregate='groups'/>
            <br/>
            <ExpansionPanel name={"ACCOUNTS"} aggregate='accounts'/>
            <br/>
            <ExpansionPanel name={"CATEGORIES"} aggregate='categories'/>
            <br/>
            <ExpansionPanel name={"TRANSACTIONS"} aggregate='transactions'/>
        </div>
    )
}

export default Home
